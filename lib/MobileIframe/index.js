'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _Iframe = require('./Iframe');

var _Iframe2 = _interopRequireDefault(_Iframe);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function createElemet(properties) {
  var divTarget = document.createElement('div');
  divTarget.id = 'box-iframe';
  document.body.appendChild(divTarget);

  function onClose() {
    if (properties.onClose) {
      properties.onClose();
    }
    close();
  }

  (0, _reactDom.render)(_react2.default.createElement(_Iframe2.default, { show: true, testID: properties.testID, title: properties.title, src: properties.src, onClose: onClose }), divTarget);
}

function open(properties) {
  close().then(function () {
    createElemet(properties || {});
  });
}

function close() {
  return new Promise(function (resolve) {
    var target = document.getElementById('box-iframe');
    if (!target) resolve();

    target.parentNode.removeChild(target);
  });
}

exports.default = {
  open: open,
  close: close
};