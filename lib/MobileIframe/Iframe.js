'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _MobileModalFullscreen = require('../MobileModalFullscreen');

var _MobileModalFullscreen2 = _interopRequireDefault(_MobileModalFullscreen);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var iOSWebView = function iOSWebView() {
  return navigator.userAgent.match(/SpaceX_iOS/i);
};

var Iframe = function Iframe(_ref) {
  var testID = _ref.testID,
      title = _ref.title,
      show = _ref.show,
      src = _ref.src,
      onClose = _ref.onClose;

  var _useState = (0, _react.useState)(show),
      _useState2 = _slicedToArray(_useState, 2),
      isShow = _useState2[0],
      setIsShow = _useState2[1];

  var _useState3 = (0, _react.useState)(false),
      _useState4 = _slicedToArray(_useState3, 2),
      showIframe = _useState4[0],
      setShowIframe = _useState4[1];

  (0, _react.useEffect)(function () {
    setTimeout(function () {
      return setShowIframe(show);
    }, 500);
  }, [show]);

  var handleClickClose = function handleClickClose() {
    setIsShow(false);
    setTimeout(function () {
      onClose();
      setShowIframe(false);
    }, 1000);
  };

  return _react2.default.createElement(
    _MobileModalFullscreen2.default,
    {
      testID: testID,
      show: isShow,
      title: title,
      onClose: handleClickClose,
      overflowHidden: !iOSWebView()
    },
    showIframe && _react2.default.createElement('iframe', {
      id: 'appIframe',
      width: '100%',
      height: '100%',
      name: 'appIframe',
      title: 'App iframe',
      src: src,
      frameBorder: '0'
    })
  );
};

Iframe.propTypes = {
  testID: _propTypes2.default.string,
  title: _propTypes2.default.string.isRequired,
  src: _propTypes2.default.string.isRequired,
  show: _propTypes2.default.bool.isRequired,
  onClose: _propTypes2.default.func.isRequired
};

Iframe.defaultProps = {
  testID: 'modal-iframe',
  title: 'Iframe Title',
  src: 'https://en.wikipedia.org',
  show: false,
  onClose: function onClose() {
    return null;
  }
};

exports.default = Iframe;