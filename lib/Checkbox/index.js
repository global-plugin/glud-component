'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp2;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Checkbox = (_temp2 = _class = function (_PureComponent) {
  _inherits(Checkbox, _PureComponent);

  function Checkbox() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Checkbox);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Checkbox.__proto__ || Object.getPrototypeOf(Checkbox)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      isChecked: _this.props.checked
    }, _this.componentWillReceiveProps = function (nextProps) {
      if (_this.state.checked !== nextProps.checked) {
        _this.setState(function () {
          return {
            isChecked: nextProps.checked
          };
        });
      }
    }, _this.handleCheckboxChange = function () {
      var _this$props = _this.props,
          onChange = _this$props.onChange,
          value = _this$props.value;

      _this.setState(function (prevState) {
        return { isChecked: !prevState.isChecked };
      });
      onChange(!_this.state.isChecked, value);
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Checkbox, [{
    key: 'render',
    value: function render() {
      var isChecked = this.state.isChecked;
      var _props = this.props,
          testID = _props.testID,
          className = _props.className,
          onlyContain = _props.onlyContain,
          name = _props.name,
          label = _props.label,
          value = _props.value,
          disabled = _props.disabled;

      var classes = (0, _classnames2.default)('is-checkradio is-primary', {
        'only-contain': onlyContain,
        'has-background-color': isChecked
      }, className);

      return _react2.default.createElement(
        'span',
        null,
        _react2.default.createElement('input', {
          'data-test-id': testID,
          name: name,
          className: classes,
          type: 'checkbox',
          value: value,
          checked: isChecked,
          readOnly: 'readOnly',
          disabled: disabled
        }),
        _react2.default.createElement(
          'label',
          { onClick: disabled ? null : this.handleCheckboxChange },
          !onlyContain && label
        )
      );
    }
  }]);

  return Checkbox;
}(_react.PureComponent), _class.propTypes = {
  testID: _propTypes2.default.string,
  className: _propTypes2.default.string,
  label: _propTypes2.default.string,
  name: _propTypes2.default.string,
  value: _propTypes2.default.oneOfType([_propTypes2.default.shape(), _propTypes2.default.string, _propTypes2.default.number]).isRequired,
  checked: _propTypes2.default.bool,
  disabled: _propTypes2.default.bool,
  onlyContain: _propTypes2.default.bool,
  /**
   * Return (isChecked, value)
   */
  onChange: _propTypes2.default.func
}, _class.defaultProps = {
  testID: 'checkox',
  label: 'checkbox',
  value: {},
  checked: false,
  onChange: function onChange(isChecked, value) {
    return null;
  }
}, _temp2);
exports.default = Checkbox;