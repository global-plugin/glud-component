'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp2;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _ModalContent = require('./ModalContent');

var _ModalContent2 = _interopRequireDefault(_ModalContent);

var _ModalFooter = require('./ModalFooter');

var _ModalFooter2 = _interopRequireDefault(_ModalFooter);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Modal = (_temp2 = _class = function (_Component) {
  _inherits(Modal, _Component);

  function Modal() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Modal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Modal.__proto__ || Object.getPrototypeOf(Modal)).call.apply(_ref, [this].concat(args))), _this), _this.getModalCardStyle = function () {
      var maxWidth = _this.props.maxWidth;

      var modalCardStyles = {
        maxWidth: maxWidth + 'px',
        width: '100%'
      };

      if (maxWidth) return modalCardStyles;
      return {};
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Modal, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          testID = _props.testID,
          className = _props.className,
          title = _props.title,
          centered = _props.centered,
          open = _props.open,
          fullscreen = _props.fullscreen,
          inline = _props.inline,
          closeOnClickOutside = _props.closeOnClickOutside,
          onClose = _props.onClose,
          children = _props.children;


      var modalClass = (0, _classnames2.default)('', {
        modal: !inline,
        'is-active': open,
        'is-centered': centered,
        fullscreen: fullscreen
      }, className);

      return _react2.default.createElement(
        'div',
        { className: modalClass },
        !inline && _react2.default.createElement('div', {
          'data-test-id': testID + '-overlay',
          className: 'modal-background',
          onClick: function onClick() {
            return closeOnClickOutside && onClose() || null;
          }
        }),
        _react2.default.createElement(
          'div',
          { className: 'modal-card', style: this.getModalCardStyle() },
          title && _react2.default.createElement(
            'header',
            { className: 'modal-card-head' },
            _react2.default.createElement(
              'p',
              { className: 'modal-card-title' },
              title
            ),
            onClose && _react2.default.createElement('button', {
              'data-test-id': testID + '-close-button',
              className: 'delete',
              onClick: onClose
            })
          ),
          children
        )
      );
    }
  }]);

  return Modal;
}(_react.Component), _class.propTypes = {
  testID: _propTypes2.default.string,
  className: _propTypes2.default.string,
  title: _propTypes2.default.string,
  centered: _propTypes2.default.bool,
  open: _propTypes2.default.bool,
  fullscreen: _propTypes2.default.bool,
  maxWidth: _propTypes2.default.number,
  inline: _propTypes2.default.bool,
  closeOnClickOutside: _propTypes2.default.bool,
  onClose: _propTypes2.default.func
}, _class.defaultProps = {
  testID: 'modal',
  closeOnClickOutside: true
}, _temp2);


Modal.Content = _ModalContent2.default;
Modal.Footer = _ModalFooter2.default;

exports.default = Modal;