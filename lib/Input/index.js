'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp2;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _cleave = require('cleave.js');

var _cleave2 = _interopRequireDefault(_cleave);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Input = (_temp2 = _class = function (_PureComponent) {
  _inherits(Input, _PureComponent);

  function Input() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Input);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Input.__proto__ || Object.getPrototypeOf(Input)).call.apply(_ref, [this].concat(args))), _this), _this.blur = function () {
      _this.input.blur();
    }, _this.focus = function () {
      _this.input.focus();
    }, _this.getInput = function () {
      return _this.input;
    }, _this.componentDidMount = function () {
      var _this$props = _this.props,
          formatOptions = _this$props.formatOptions,
          onChange = _this$props.onChange;

      if (formatOptions) {
        var options = _extends({}, formatOptions, {
          onValueChanged: onChange
        });
        _this.cleave = new _cleave2.default(_this.input, options);
      }
    }, _this.componentWillUnmount = function () {
      var formatOptions = _this.props.formatOptions;

      if (formatOptions) {
        _this.cleave.destroy();
      }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Input, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          testID = _props.testID,
          className = _props.className,
          onlyContain = _props.onlyContain,
          type = _props.type,
          label = _props.label,
          name = _props.name,
          defaultValue = _props.defaultValue,
          value = _props.value,
          autoComplete = _props.autoComplete,
          maxLength = _props.maxLength,
          isRequired = _props.isRequired,
          isSuccess = _props.isSuccess,
          isError = _props.isError,
          message = _props.message,
          placeholder = _props.placeholder,
          onChange = _props.onChange,
          onFocus = _props.onFocus,
          onBlur = _props.onBlur,
          onKeyDown = _props.onKeyDown,
          onKeyUp = _props.onKeyUp,
          onKeyPress = _props.onKeyPress,
          disabled = _props.disabled,
          readOnly = _props.readOnly,
          loading = _props.loading,
          iconLeft = _props.iconLeft,
          iconRight = _props.iconRight,
          large = _props.large,
          medium = _props.medium,
          small = _props.small;


      var status = {
        'is-success': isSuccess,
        'is-danger': isError,
        'is-large': large,
        'is-medium': medium,
        'is-small': small
      };

      var classInput = (0, _classnames2.default)('input', status);
      var classHelp = (0, _classnames2.default)('help', status);
      var classControl = (0, _classnames2.default)('control', {
        'is-loading': loading,
        'has-icons-left': iconLeft,
        'has-icons-right': iconRight
      }, className);

      var inputField = _react2.default.createElement('input', {
        'data-test-id': testID,
        ref: function ref(input) {
          return _this2.input = input;
        },
        className: classInput,
        type: type,
        name: name,
        defaultValue: defaultValue,
        value: value,
        maxLength: maxLength,
        placeholder: placeholder,
        onChange: onChange,
        onFocus: onFocus,
        onBlur: onBlur,
        onKeyDown: onKeyDown,
        onKeyUp: onKeyUp,
        onKeyPress: onKeyPress,
        disabled: disabled,
        readOnly: readOnly,
        autoComplete: autoComplete
      });

      var iconL = null;
      if (iconLeft) {
        iconL = _react2.default.createElement(
          'span',
          { className: 'icon is-left' },
          iconLeft()
        );
      }

      var iconR = null;
      if (iconRight) {
        iconR = _react2.default.createElement(
          'span',
          { className: 'icon is-right' },
          iconRight()
        );
      }

      if (onlyContain) {
        return _react2.default.createElement(
          'div',
          { className: classControl },
          inputField,
          iconL,
          iconR
        );
      }

      return _react2.default.createElement(
        'div',
        { className: 'field' },
        label && _react2.default.createElement(
          'label',
          { className: 'label' },
          label,
          ' ',
          isRequired && _react2.default.createElement('i', { className: 'required fas fa-asterisk' })
        ),
        _react2.default.createElement(
          'div',
          { className: classControl },
          inputField,
          iconL,
          iconR
        ),
        message && _react2.default.createElement(
          'p',
          { className: classHelp },
          message
        )
      );
    }
  }]);

  return Input;
}(_react.PureComponent), _class.propTypes = {
  testID: _propTypes2.default.string,
  className: _propTypes2.default.string,
  onlyContain: _propTypes2.default.bool,
  type: _propTypes2.default.string,
  label: _propTypes2.default.string,
  name: _propTypes2.default.string,
  defaultValue: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
  value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
  /**
   * Using Cleave.js https://github.com/nosir/cleave.js/blob/master/doc/options.md
   */
  formatOptions: _propTypes2.default.shape({}),
  autoComplete: _propTypes2.default.string,
  maxLength: _propTypes2.default.number,
  isRequired: _propTypes2.default.bool,
  isSuccess: _propTypes2.default.bool,
  isError: _propTypes2.default.bool,
  message: _propTypes2.default.string,
  placeholder: _propTypes2.default.string,
  onChange: _propTypes2.default.func,
  onFocus: _propTypes2.default.func,
  onBlur: _propTypes2.default.func,
  onKeyDown: _propTypes2.default.func,
  onKeyUp: _propTypes2.default.func,
  onKeyPress: _propTypes2.default.func,
  disabled: _propTypes2.default.bool,
  readOnly: _propTypes2.default.bool,
  loading: _propTypes2.default.bool,
  iconLeft: _propTypes2.default.func,
  iconRight: _propTypes2.default.func,
  large: _propTypes2.default.bool,
  medium: _propTypes2.default.bool,
  small: _propTypes2.default.bool
}, _class.defaultProps = {
  testID: 'input',
  type: 'text',
  autoComplete: 'off',
  onChange: function onChange() {
    return null;
  },
  onFocus: function onFocus() {
    return null;
  },
  onBlur: function onBlur() {
    return null;
  },
  onKeyDown: function onKeyDown() {
    return null;
  },
  onKeyUp: function onKeyUp() {
    return null;
  },
  onKeyPress: function onKeyPress() {
    return null;
  }
}, _temp2);
exports.default = Input;