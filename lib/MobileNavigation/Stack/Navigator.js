'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _templateObject = _taggedTemplateLiteral(['\n  label: StackNavigator;\n\n  position: relative;\n  flex-grow: 1;\n  display: flex;\n  flex-direction: column;\n'], ['\n  label: StackNavigator;\n\n  position: relative;\n  flex-grow: 1;\n  display: flex;\n  flex-direction: column;\n']);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styled = require('@emotion/styled');

var _styled2 = _interopRequireDefault(_styled);

var _get = require('lodash/get');

var _get2 = _interopRequireDefault(_get);

var _web = require('react-spring/web.cjs');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var createStateBackup = function createStateBackup() {
  var state = {};
  return {
    watch: function watch() {
      return state;
    },
    get: function get(key) {
      return state[key];
    },
    set: function set(key, value) {
      return state[key] = value;
    }
  };
};

var stateBackup = createStateBackup();

var getKey = function getKey(data) {
  return data.reduce(function (result, item) {
    return result + '.' + item.props.name;
  }, 'Stack');
};

var Navigator = function Navigator(props) {
  var stackKey = getKey(props.children);
  var getDefaultScreen = function getDefaultScreen(_ref) {
    var _ref$useBackup = _ref.useBackup,
        useBackup = _ref$useBackup === undefined ? true : _ref$useBackup;

    var statePrevios = stateBackup.get(stackKey);
    if (useBackup && statePrevios && statePrevios.length > 0) return statePrevios;

    if (props.children.length === 0) return [];

    if (props.initialScreen) {
      var initialScreenData = {
        name: props.initialScreen,
        params: {}
      };
      return [initialScreenData];
    }

    var firstScreen = (0, _get2.default)(props, 'children[0].props.name', null);
    if (firstScreen) {
      var screenData = {
        name: firstScreen,
        params: {}
      };
      return [screenData];
    }

    console.error('Please define screen name');
    return [];
  };

  var _useState = (0, _react.useState)(getDefaultScreen({ useBackup: true })),
      _useState2 = _slicedToArray(_useState, 2),
      screenStack = _useState2[0],
      setScreenStack = _useState2[1];

  (0, _react.useEffect)(function () {
    setScreenStack(getDefaultScreen({ useBackup: false }));
  }, [props.initialScreen]);

  (0, _react.useEffect)(function () {
    stateBackup.set(stackKey, screenStack);
  }, [screenStack, stackKey]);

  var navigate = function navigate(screenName) {
    var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    var isChildrenScreen = props.children.some(function (child) {
      return child.props.name === screenName;
    });
    if (!isChildrenScreen) {
      console.error('Screen (' + screenName + ') is not found in navigator');
      return;
    }

    var newScreen = { name: screenName, params: params };
    var isExisting = screenStack.some(function (screen) {
      return screen.name === screenName;
    });

    var getPreviosScreens = function getPreviosScreens() {
      var previosScreens = [];
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = screenStack[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var screen = _step.value;

          if (screen.name === screenName) {
            previosScreens.push(newScreen);
            break;
          }
          previosScreens.push(screen);
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      return previosScreens;
    };

    if (isExisting) {
      setScreenStack(getPreviosScreens());
    } else {
      setScreenStack([].concat(_toConsumableArray(screenStack), [newScreen]));
    }
  };

  var push = function push(screenName) {
    var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    var isChildrenScreen = props.children.some(function (child) {
      return child.props.name === screenName;
    });
    if (!isChildrenScreen) {
      console.error('Screen (' + screenName + ') is not found in navigator');
      return;
    }

    var newScreen = { name: screenName, params: params };
    setScreenStack([].concat(_toConsumableArray(screenStack), [newScreen]));
  };

  var replace = function replace(index) {
    return function (screenName) {
      var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      var newScreen = { name: screenName, params: params };
      var newScreenStack = screenStack.filter(function (stack, i) {
        return i !== index;
      });
      setScreenStack([].concat(_toConsumableArray(newScreenStack), [newScreen]));
    };
  };

  var goBack = function goBack(index) {
    return function () {
      if (screenStack.length === 1) return;

      var nextScreenStack = screenStack.filter(function (stack, i) {
        return i !== index;
      });
      setScreenStack(nextScreenStack);
    };
  };

  var setParams = function setParams(index) {
    return function (params) {
      var nextScreenStack = screenStack.map(function (stack, i) {
        if (i === index) {
          return _extends({}, stack, {
            params: _extends({}, stack.params, { params: params })
          });
        }

        return stack;
      });
      setScreenStack(nextScreenStack);
    };
  };

  var reset = function reset() {
    stateBackup.set(stackKey, null);
    setScreenStack(getDefaultScreen());
  };

  var getScreen = function getScreen(screenName) {
    var screenToRender = props.children.find(function (child) {
      return child.props.name === screenName;
    });
    if (screenToRender) return screenToRender;
    console.error('Screen is undefined in stack');
    return null;
  };

  var transitions = (0, _web.useTransition)(screenStack, function (item) {
    return item.name;
  }, {
    from: function from(item) {
      return {
        willChange: 'all',
        position: 'absolute',
        display: 'flex',
        flexDirection: 'column',
        zIndex: 1,
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        overflow: 'hidden',
        transform: screenStack.length === 1 ? 'translate3d(0%, 0, 0)' : 'translate3d(100%, 0, 0)',
        backgroundColor: '#fff',
        boxShadow: '2px 0 5px rgba(0, 0, 0, 0.1)'
      };
    },
    update: function update(item) {
      var isLastScreen = function isLastScreen() {
        var currentScreenName = screenStack[screenStack.length - 1].name;
        if (screenStack.length === 1) return true;
        if (item.name === currentScreenName) return true;
        return false;
      };

      if (isLastScreen()) {
        return {
          transform: 'translate3d(0%, 0, 0)'
        };
      }

      return {
        transform: 'translate3d(-10%, 0, 0)'
      };
    },
    enter: {
      transform: 'translate3d(0%, 0, 0)'
    },
    leave: {
      transform: 'translate3d(100%, 0, 0)'
    },
    config: { mass: 1, tension: 1000, friction: 70 }
  });

  return _react2.default.createElement(
    Style,
    null,
    transitions.map(function (_ref2, i) {
      var item = _ref2.item,
          key = _ref2.key,
          props = _ref2.props;

      var screenToRender = getScreen(item.name);
      var navigationProps = {
        navigate: navigate,
        push: push,
        replace: replace(i),
        goBack: goBack(i),
        setParams: setParams(i),
        reset: reset
      };
      var propsToInject = _extends({
        key: item.name,
        isMain: i === 0
      }, screenToRender.props, {
        params: item.params,
        navigation: navigationProps
      });
      var screen = _react2.default.cloneElement(screenToRender, propsToInject);

      if (item) {
        return _react2.default.createElement(
          _web.animated.div,
          { key: key, style: props },
          screen
        );
      }

      return null;
    })
  );
};

Navigator.propTypes = {
  initialScreen: _propTypes2.default.string
};

Navigator.defaultProps = {
  initialScreen: null
};

exports.default = Navigator;


var Style = _styled2.default.div(_templateObject);