'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _templateObject = _taggedTemplateLiteral(['\n  label: StackScreen;\n\n  display: flex;\n  flex-direction: column;\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background: #fff;\n  overflow: hidden;\n\n  > .app-header {\n    flex-shrink: 0;\n  }\n\n  > div {\n    flex-grow: 1;\n    overflow: auto;\n  }\n'], ['\n  label: StackScreen;\n\n  display: flex;\n  flex-direction: column;\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background: #fff;\n  overflow: hidden;\n\n  > .app-header {\n    flex-shrink: 0;\n  }\n\n  > div {\n    flex-grow: 1;\n    overflow: auto;\n  }\n']);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styled = require('@emotion/styled');

var _styled2 = _interopRequireDefault(_styled);

var _AppHeader = require('../../AppHeader');

var _AppHeader2 = _interopRequireDefault(_AppHeader);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Screen = function Screen(props) {
  var name = props.name,
      isMain = props.isMain,
      navigation = props.navigation,
      component = props.component,
      options = props.options;

  var _useState = (0, _react.useState)(options),
      _useState2 = _slicedToArray(_useState, 2),
      screenOptions = _useState2[0],
      setScreenOption = _useState2[1];

  var headerDisabled = screenOptions.headerDisabled,
      title = screenOptions.title,
      subTitle = screenOptions.subTitle,
      titleLeft = screenOptions.titleLeft,
      rightActions = screenOptions.rightActions,
      headerRenderer = screenOptions.headerRenderer;

  var handleSetScreenOption = function handleSetScreenOption(newOptions) {
    setScreenOption(_extends({}, screenOptions, newOptions));
  };

  var propsToComponent = {
    initialScreen: props.params.screen || null,
    params: props.params,
    navigation: _extends({}, navigation, { setOptions: handleSetScreenOption })
  };

  var header = function header() {
    if (headerRenderer) return headerRenderer(screenOptions);
    if (isMain) return null;
    if (headerDisabled) return null;

    return _react2.default.createElement(_AppHeader2.default, {
      testID: name,
      title: title || name,
      subTitle: subTitle,
      titleLeft: titleLeft,
      leftIcon: 'fas fa-arrow-left',
      onClickLeft: function onClickLeft() {
        return navigation.goBack();
      },
      rightActions: rightActions
    });
  };

  var renderComponent = function renderComponent() {
    if (typeof component === 'function') return component(propsToComponent);
    var ComponentToRender = component;
    return _react2.default.createElement(ComponentToRender, propsToComponent);
  };

  return _react2.default.createElement(
    Style,
    null,
    header(),
    renderComponent()
  );
};

Screen.propTypes = {
  name: _propTypes2.default.string.isRequired,
  component: _propTypes2.default.oneOfType([_propTypes2.default.func, _propTypes2.default.shape({})]).isRequired,
  options: _propTypes2.default.shape({})
};

Screen.defaultProps = {
  options: {}
};

exports.default = Screen;


var Style = _styled2.default.div(_templateObject);