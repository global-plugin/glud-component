'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _templateObject = _taggedTemplateLiteral(['\n  label: NavigationContainer;\n\n  height: 100%;\n  display: flex;\n  flex-direction: column;\n  overflow: hidden;\n'], ['\n  label: NavigationContainer;\n\n  height: 100%;\n  display: flex;\n  flex-direction: column;\n  overflow: hidden;\n']);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styled = require('@emotion/styled');

var _styled2 = _interopRequireDefault(_styled);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); } // import PropTypes from 'prop-types'


var NavigationContainer = function NavigationContainer(props) {
  return _react2.default.createElement(
    Style,
    null,
    props.children
  );
};

NavigationContainer.propTypes = {};

NavigationContainer.defaultProps = {};

exports.default = NavigationContainer;


var Style = (0, _styled2.default)('div')(_templateObject);