'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _templateObject = _taggedTemplateLiteral(['\n  label: TabScreen;\n\n  display: flex;\n  flex-direction: column;\n  flex-grow: 1;\n  overflow: hidden;\n\n  > div {\n    flex-grow: 1;\n    overflow: auto;\n  }\n'], ['\n  label: TabScreen;\n\n  display: flex;\n  flex-direction: column;\n  flex-grow: 1;\n  overflow: hidden;\n\n  > div {\n    flex-grow: 1;\n    overflow: auto;\n  }\n']);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styled = require('@emotion/styled');

var _styled2 = _interopRequireDefault(_styled);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Screen = function Screen(props) {
  var navigation = props.navigation,
      component = props.component;


  var propsToComponent = {
    navigation: navigation
  };

  return _react2.default.createElement(
    Style,
    null,
    component(propsToComponent)
  );
};

Screen.propTypes = {
  name: _propTypes2.default.string.isRequired,
  component: _propTypes2.default.func.isRequired
};

Screen.defaultProps = {};

exports.default = Screen;


var Style = _styled2.default.div(_templateObject);