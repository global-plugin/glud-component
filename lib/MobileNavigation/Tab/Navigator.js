'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _templateObject = _taggedTemplateLiteral(['\n  label: TabNavigator;\n\n  position: relative;\n  flex-grow: 1;\n  display: flex;\n  flex-direction: column;\n  height: 100%;\n\n  > .box-tab {\n    position: relative;\n    box-shadow: 0 -1px 2px rgba(10, 10, 10, 0.1);\n\n    > .tabs {\n      font-size: 0.75rem;\n\n      a {\n        flex-direction: column;\n        > .icon {\n          margin: 0;\n        }\n      }\n    }\n  }\n'], ['\n  label: TabNavigator;\n\n  position: relative;\n  flex-grow: 1;\n  display: flex;\n  flex-direction: column;\n  height: 100%;\n\n  > .box-tab {\n    position: relative;\n    box-shadow: 0 -1px 2px rgba(10, 10, 10, 0.1);\n\n    > .tabs {\n      font-size: 0.75rem;\n\n      a {\n        flex-direction: column;\n        > .icon {\n          margin: 0;\n        }\n      }\n    }\n  }\n']);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styled = require('@emotion/styled');

var _styled2 = _interopRequireDefault(_styled);

var _findIndex = require('lodash/findIndex');

var _findIndex2 = _interopRequireDefault(_findIndex);

var _Tab = require('../../Tab');

var _Tab2 = _interopRequireDefault(_Tab);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Stack = function Stack(props) {
  var getDefaultScreen = function getDefaultScreen() {
    if (props.children.length === 0) {
      console.error('Please add Tab Screen');
    }

    if (props.initialScreen) return props.initialScreen;

    return props.children[0].props.name;
  };

  var _useState = (0, _react.useState)(getDefaultScreen()),
      _useState2 = _slicedToArray(_useState, 2),
      tabActive = _useState2[0],
      setTabActive = _useState2[1];

  var handleClickTab = function handleClickTab(tabIndex, tabName) {
    setTabActive(tabName);
  };

  var renderScreenActive = function renderScreenActive() {
    var screen = props.children.find(function (child) {
      return child.props.name === tabActive;
    });
    return _react2.default.cloneElement(screen, _extends({ key: Date.now() }, screen.props));
  };

  var getTabIndex = function getTabIndex() {
    return (0, _findIndex2.default)(props.children, function (child) {
      return child.props.name === tabActive;
    }) + 1;
  };

  return _react2.default.createElement(
    Style,
    null,
    renderScreenActive(),
    _react2.default.createElement(
      _Tab2.default,
      { defaultActive: getTabIndex(), onClick: handleClickTab, fullwidthEqualSized: true },
      props.children.map(function (child, i) {
        return _react2.default.createElement(_Tab2.default.Item, _extends({ key: i }, child.props));
      })
    )
  );
};

Stack.propTypes = {
  initialScreen: _propTypes2.default.string // eslint-disable-line
};

Stack.defaultProps = {
  initialScreen: null
};

exports.default = Stack;


var Style = _styled2.default.div(_templateObject);