'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Navigator = require('./Navigator');

var _Navigator2 = _interopRequireDefault(_Navigator);

var _Screen = require('./Screen');

var _Screen2 = _interopRequireDefault(_Screen);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = { Navigator: _Navigator2.default, Screen: _Screen2.default };