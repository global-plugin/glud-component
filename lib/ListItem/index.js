'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp2;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ListItem = (_temp2 = _class = function (_PureComponent) {
  _inherits(ListItem, _PureComponent);

  function ListItem() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, ListItem);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = ListItem.__proto__ || Object.getPrototypeOf(ListItem)).call.apply(_ref, [this].concat(args))), _this), _this.handleClickMenu = function () {
      var onClick = _this.props.onClick;

      onClick();
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(ListItem, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          testID = _props.testID,
          className = _props.className,
          label = _props.label,
          desc = _props.desc,
          icon = _props.icon,
          arrow = _props.arrow,
          children = _props.children;


      var menuClass = (0, _classnames2.default)('list-view-item', {
        'has-arrow': arrow
      }, className);

      return _react2.default.createElement(
        'div',
        { 'data-test-id': testID, className: menuClass, onClick: this.handleClickMenu },
        icon && _react2.default.createElement(
          'div',
          { className: 'logo' },
          _react2.default.createElement('img', { src: icon })
        ),
        _react2.default.createElement(
          'div',
          { className: 'box-text' },
          children,
          !children && _react2.default.createElement(
            _react.Fragment,
            null,
            _react2.default.createElement(
              'div',
              { className: 'is-6' },
              label
            ),
            _react2.default.createElement(
              'span',
              { className: 'is-size-7 has-text-grey' },
              desc
            )
          )
        ),
        arrow && _react2.default.createElement('i', { className: 'fas fa-chevron-right' })
      );
    }
  }]);

  return ListItem;
}(_react.PureComponent), _class.propTypes = {
  testID: _propTypes2.default.string,
  className: _propTypes2.default.string,
  label: _propTypes2.default.string,
  desc: _propTypes2.default.string,
  icon: _propTypes2.default.string,
  arrow: _propTypes2.default.bool,
  onClick: _propTypes2.default.func
}, _class.defaultProps = {
  testID: 'item-list',
  arrow: false,
  onClick: function onClick() {
    return null;
  }
}, _temp2);
exports.default = ListItem;