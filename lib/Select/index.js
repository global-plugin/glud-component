'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Select = (_temp = _class = function (_Component) {
  _inherits(Select, _Component);

  function Select() {
    _classCallCheck(this, Select);

    return _possibleConstructorReturn(this, (Select.__proto__ || Object.getPrototypeOf(Select)).apply(this, arguments));
  }

  _createClass(Select, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          testID = _props.testID,
          className = _props.className,
          onlyContain = _props.onlyContain,
          inline = _props.inline,
          name = _props.name,
          label = _props.label,
          defaultValue = _props.defaultValue,
          value = _props.value,
          isRequired = _props.isRequired,
          isSuccess = _props.isSuccess,
          isError = _props.isError,
          message = _props.message,
          onChange = _props.onChange,
          onFocus = _props.onFocus,
          onBlur = _props.onBlur,
          disabled = _props.disabled,
          loading = _props.loading,
          iconLeft = _props.iconLeft,
          options = _props.options;


      var status = {
        'is-success': isSuccess,
        'is-danger': isError,
        'is-loading': loading
      };

      var classSelect = (0, _classnames2.default)('select', status, {
        inline: inline
      });
      var classHelp = (0, _classnames2.default)('help', status);
      var classControl = (0, _classnames2.default)('control', {
        'is-loading': loading,
        'has-icons-left': iconLeft
      });

      var inputField = _react2.default.createElement(
        'div',
        { className: classSelect },
        _react2.default.createElement(
          'select',
          {
            name: name,
            'data-test-id': testID,
            onChange: onChange,
            onFocus: onFocus,
            onBlur: onBlur,
            defaultValue: defaultValue,
            value: value,
            disabled: disabled
          },
          options.map(function (option, i) {
            return _react2.default.createElement(
              'option',
              { key: i, value: option.value, disabled: option.disabled },
              option.label
            );
          })
        )
      );

      var iconL = null;
      if (iconLeft) {
        iconL = _react2.default.createElement(
          'span',
          { className: 'icon is-left' },
          iconLeft()
        );
      }

      if (onlyContain) {
        return _react2.default.createElement(
          'div',
          { className: classControl },
          inputField,
          iconL
        );
      }

      return _react2.default.createElement(
        'div',
        { className: 'field ' + className },
        _react2.default.createElement(
          'label',
          { className: 'label' },
          label,
          ' ',
          isRequired && _react2.default.createElement('i', { className: 'required fas fa-asterisk' })
        ),
        _react2.default.createElement(
          'div',
          { className: classControl },
          inputField,
          iconL
        ),
        message && _react2.default.createElement(
          'p',
          { className: classHelp },
          message
        )
      );
    }
  }]);

  return Select;
}(_react.Component), _class.propTypes = {
  testID: _propTypes2.default.string,
  className: _propTypes2.default.string,
  onlyContain: _propTypes2.default.bool,
  inline: _propTypes2.default.bool,
  name: _propTypes2.default.string,
  label: _propTypes2.default.string,
  defaultValue: _propTypes2.default.oneOfType([_propTypes2.default.shape(), _propTypes2.default.array]),
  value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
  isRequired: _propTypes2.default.bool,
  isSuccess: _propTypes2.default.bool,
  isError: _propTypes2.default.bool,
  message: _propTypes2.default.string,
  onChange: _propTypes2.default.func,
  onFocus: _propTypes2.default.func,
  onBlur: _propTypes2.default.func,
  disabled: _propTypes2.default.bool,
  loading: _propTypes2.default.bool,
  iconLeft: _propTypes2.default.func,
  options: _propTypes2.default.array
}, _class.defaultProps = {
  testID: 'select',
  options: [{
    label: 'Select dropdown',
    value: 0
  }, {
    label: 'Option 1',
    value: 1
  }]
}, _temp);
exports.default = Select;