'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Button = (_temp = _class = function (_React$PureComponent) {
  _inherits(Button, _React$PureComponent);

  function Button() {
    _classCallCheck(this, Button);

    return _possibleConstructorReturn(this, (Button.__proto__ || Object.getPrototypeOf(Button)).apply(this, arguments));
  }

  _createClass(Button, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          testID = _props.testID,
          className = _props.className,
          title = _props.title,
          primary = _props.primary,
          secondary = _props.secondary,
          isStatic = _props.isStatic,
          fullwidth = _props.fullwidth,
          large = _props.large,
          medium = _props.medium,
          small = _props.small,
          loading = _props.loading,
          active = _props.active,
          outlined = _props.outlined,
          disabled = _props.disabled,
          icon = _props.icon,
          rightIcon = _props.rightIcon,
          tooltip = _props.tooltip,
          tooltipMultiline = _props.tooltipMultiline,
          tooltipLeft = _props.tooltipLeft,
          tooltipRight = _props.tooltipRight,
          tooltipBottom = _props.tooltipBottom,
          onClick = _props.onClick,
          children = _props.children;


      var classes = (0, _classnames2.default)('button', {
        tooltip: tooltip,
        'is-tooltip-multiline': tooltipMultiline,
        'is-tooltip-right': tooltipRight,
        'is-tooltip-left': tooltipLeft,
        'is-tooltip-bottom': tooltipBottom,
        'is-primary': primary,
        'is-dark': secondary,
        'is-static': isStatic,
        'is-large': large,
        'is-medium': medium,
        'is-outlined': outlined,
        'is-small': small,
        'is-active': active,
        'is-loading': loading,
        'is-fullwidth': fullwidth
      }, className);

      return _react2.default.createElement(
        'button',
        {
          'data-test-id': testID,
          'data-tooltip': tooltip,
          className: classes,
          disabled: disabled,
          onClick: onClick,
          title: title
        },
        icon ? _react2.default.createElement(Icon, { icon: icon }) : null,
        (icon || rightIcon) && children ? _react2.default.createElement(
          'span',
          null,
          children
        ) : children,
        rightIcon ? _react2.default.createElement(Icon, { icon: rightIcon }) : null
      );
    }
  }]);

  return Button;
}(_react2.default.PureComponent), _class.propTypes = {
  testID: _propTypes2.default.string,
  className: _propTypes2.default.string,
  title: _propTypes2.default.string,
  primary: _propTypes2.default.bool,
  secondary: _propTypes2.default.bool,
  isStatic: _propTypes2.default.bool,
  fullwidth: _propTypes2.default.bool,
  large: _propTypes2.default.bool,
  medium: _propTypes2.default.bool,
  small: _propTypes2.default.bool,
  loading: _propTypes2.default.bool,
  active: _propTypes2.default.bool,
  outlined: _propTypes2.default.bool,
  disabled: _propTypes2.default.bool,
  icon: _propTypes2.default.string,
  rightIcon: _propTypes2.default.string,
  tooltip: _propTypes2.default.string,
  tooltipMultiline: _propTypes2.default.bool,
  tooltipLeft: _propTypes2.default.bool,
  tooltipRight: _propTypes2.default.bool,
  tooltipBottom: _propTypes2.default.bool,
  onClick: _propTypes2.default.func
}, _class.defaultProps = {
  testID: 'button'
}, _temp);
exports.default = Button;

var Icon = function (_React$PureComponent2) {
  _inherits(Icon, _React$PureComponent2);

  function Icon() {
    _classCallCheck(this, Icon);

    return _possibleConstructorReturn(this, (Icon.__proto__ || Object.getPrototypeOf(Icon)).apply(this, arguments));
  }

  _createClass(Icon, [{
    key: 'render',
    value: function render() {
      var icon = this.props.icon;

      return _react2.default.createElement(
        'span',
        { className: 'icon' },
        _react2.default.createElement('i', { className: icon })
      );
    }
  }]);

  return Icon;
}(_react2.default.PureComponent);