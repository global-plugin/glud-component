'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _templateObject = _taggedTemplateLiteral(['\n  html {\n    background: #f5f5f5;\n    overflow: hidden;\n  }\n  body {\n    background: #f5f5f5;\n    overflow-y: hidden;\n    color: #4a4a4a;\n    display: flex;\n    flex-direction: column;\n  }\n  #app,\n  #root {\n    height: calc(', 'px * 100);\n    flex-grow: 1;\n    flex-direction: column;\n    display: flex;\n  }\n  .tabs.is-fullwidth li {\n    background: #fff;\n  }\n'], ['\n  html {\n    background: #f5f5f5;\n    overflow: hidden;\n  }\n  body {\n    background: #f5f5f5;\n    overflow-y: hidden;\n    color: #4a4a4a;\n    display: flex;\n    flex-direction: column;\n  }\n  #app,\n  #root {\n    height: calc(', 'px * 100);\n    flex-grow: 1;\n    flex-direction: column;\n    display: flex;\n  }\n  .tabs.is-fullwidth li {\n    background: #fff;\n  }\n']);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _core = require('@emotion/core');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var globalStyles = (0, _core.css)(_templateObject, window.innerHeight * 0.01);

exports.default = function (props) {
  return _react2.default.createElement(_core.Global, { styles: globalStyles });
};