'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp2;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _ButtonGroup = require('../ButtonGroup');

var _ButtonGroup2 = _interopRequireDefault(_ButtonGroup);

var _Button = require('../Button');

var _Button2 = _interopRequireDefault(_Button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var RadioButton = (_temp2 = _class = function (_PureComponent) {
  _inherits(RadioButton, _PureComponent);

  function RadioButton() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, RadioButton);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = RadioButton.__proto__ || Object.getPrototypeOf(RadioButton)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      value: ''
    }, _this.componentDidMount = function () {
      _this.setState(function () {
        return {
          value: _this.props.value
        };
      });
    }, _this.componentWillReceiveProps = function (nextProps) {
      _this.setState(function () {
        return {
          value: nextProps.value
        };
      });
    }, _this.isChecked = function (optionValue) {
      var sValue = _this.state.value;
      var value = _this.props.value;
      var currentValue = sValue !== '' ? sValue : value;
      return currentValue === optionValue;
    }, _this.handleButtonClick = function (value) {
      _this.setState({
        value: value
      });
      _this.props.onChange(value);
    }, _this.renderRadio = function (option, index) {
      return _react2.default.createElement(
        _Button2.default,
        {
          key: option.value,
          testID: _this.props.testID + '-' + index,
          primary: _this.isChecked(option.value),
          disabled: option.disabled,
          onClick: function onClick() {
            return _this.handleButtonClick(option.value);
          }
        },
        option.label
      );
    }, _this.renderRadioOptions = function () {
      var options = _this.props.options;

      return options.map(function (option, index) {
        return _this.renderRadio(option, index);
      });
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(RadioButton, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          className = _props.className,
          onlyContain = _props.onlyContain,
          labelGroup = _props.labelGroup,
          isRequired = _props.isRequired,
          fullwidth = _props.fullwidth,
          fullwidthEqualSized = _props.fullwidthEqualSized;


      if (onlyContain) {
        return _react2.default.createElement(
          _ButtonGroup2.default,
          { hasAddons: true },
          this.renderRadioOptions()
        );
      }

      return _react2.default.createElement(
        'div',
        { className: 'field ' + className },
        labelGroup && _react2.default.createElement(
          'label',
          { className: 'label' },
          labelGroup,
          ' ',
          isRequired && _react2.default.createElement('i', { className: 'required fas fa-asterisk' })
        ),
        _react2.default.createElement(
          _ButtonGroup2.default,
          { hasAddons: true, fullwidth: fullwidth, fullwidthEqualSized: fullwidthEqualSized },
          this.renderRadioOptions()
        )
      );
    }
  }]);

  return RadioButton;
}(_react.PureComponent), _class.propTypes = {
  testID: _propTypes2.default.string,
  className: _propTypes2.default.string,
  onlyContain: _propTypes2.default.bool,
  fullwidth: _propTypes2.default.bool,
  fullwidthEqualSized: _propTypes2.default.bool,
  labelGroup: _propTypes2.default.string,
  isRequired: _propTypes2.default.bool,
  options: _propTypes2.default.array.isRequired,
  value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]).isRequired,
  onChange: _propTypes2.default.func
}, _class.defaultProps = {
  testID: 'radio-button',
  inline: false,
  options: [],
  value: [],
  onChange: function onChange(value) {
    return null;
  }
}, _temp2);
exports.default = RadioButton;