'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp2;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// import Checkbox from '../Checkbox'

var Radio = (_temp2 = _class = function (_Component) {
  _inherits(Radio, _Component);

  function Radio() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Radio);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Radio.__proto__ || Object.getPrototypeOf(Radio)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      value: ''
    }, _this.componentDidMount = function () {
      _this.setState(function () {
        return {
          value: _this.props.value
        };
      });
    }, _this.componentWillReceiveProps = function (nextProps) {
      _this.setState(function () {
        return {
          value: nextProps.value
        };
      });
    }, _this.handleRadioClick = function (value) {
      var onChange = _this.props.onChange;

      _this.setState(function (prevState) {
        return { value: value };
      });
      onChange(value);
    }, _this.isChecked = function (optionValue) {
      if (_this.props.onlyContain) {
        if (_this.props.checked) return true;
        return false;
      }

      var sValue = _this.state.value;
      var value = _this.props.value;
      var currentValue = sValue !== '' ? sValue : value;
      return currentValue === optionValue;
    }, _this.renderRadio = function () {
      var option = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var _option$label = option.label,
          label = _option$label === undefined ? '' : _option$label,
          _option$value = option.value,
          value = _option$value === undefined ? '' : _option$value;
      var _this$props = _this.props,
          onlyContain = _this$props.onlyContain,
          disabled = _this$props.disabled;

      var isDisabled = function isDisabled() {
        if (onlyContain) {
          if (disabled) return true;
          return false;
        }

        return option.disabled;
      };

      return _react2.default.createElement(
        'span',
        { key: !onlyContain && value },
        _react2.default.createElement('input', {
          type: 'radio',
          className: 'is-checkradio is-primary ' + (onlyContain && 'only-contain'),
          name: _this.props.name,
          checked: _this.isChecked(value),
          readOnly: 'readOnly',
          disabled: isDisabled()
        }),
        _react2.default.createElement(
          'label',
          {
            'data-test-id': _this.props.name + '-' + index,
            onClick: function onClick() {
              return isDisabled() ? null : _this.handleRadioClick(value);
            }
          },
          !onlyContain && label
        )
      );
    }, _this.renderRadioOptions = function () {
      var _this$props2 = _this.props,
          inline = _this$props2.inline,
          options = _this$props2.options;


      if (inline) return options.map(function (option, index) {
        return _this.renderRadio(option, index);
      });

      return options.map(function (option, index) {
        return _react2.default.createElement(
          'div',
          { key: index, className: 'box-field' },
          _this.renderRadio(option, index)
        );
      });
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Radio, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          className = _props.className,
          onlyContain = _props.onlyContain,
          labelGroup = _props.labelGroup,
          isRequired = _props.isRequired;


      if (onlyContain) return this.renderRadio();

      return _react2.default.createElement(
        'div',
        { className: 'field ' + className },
        labelGroup && _react2.default.createElement(
          'label',
          { className: 'label' },
          labelGroup,
          ' ',
          isRequired && _react2.default.createElement('i', { className: 'required fas fa-asterisk' })
        ),
        this.renderRadioOptions()
      );
    }
  }]);

  return Radio;
}(_react.Component), _class.propTypes = {
  className: _propTypes2.default.string,
  labelGroup: _propTypes2.default.string,
  name: _propTypes2.default.string.isRequired,
  isRequired: _propTypes2.default.bool,
  inline: _propTypes2.default.bool,
  options: _propTypes2.default.array.isRequired,
  value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]).isRequired,
  /**
   * Only work with onlyContain
   */
  disabled: _propTypes2.default.bool,
  /**
   * Only work with onlyContain
   */
  checked: _propTypes2.default.bool,
  onlyContain: _propTypes2.default.bool,
  onChange: _propTypes2.default.func
}, _class.defaultProps = {
  inline: false,
  options: [],
  value: [],
  onChange: function onChange(value) {
    return null;
  }
}, _temp2);
exports.default = Radio;