'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp2;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _TabContent = require('./TabContent');

var _TabContent2 = _interopRequireDefault(_TabContent);

var _TabItem = require('./TabItem');

var _TabItem2 = _interopRequireDefault(_TabItem);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Tab = (_temp2 = _class = function (_PureComponent) {
  _inherits(Tab, _PureComponent);

  function Tab() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Tab);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Tab.__proto__ || Object.getPrototypeOf(Tab)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      tabActive: 1
    }, _this.componentDidMount = function () {
      _this.setState({
        tabActive: _this.props.defaultActive
      });
    }, _this.onClickTab = function (_ref2) {
      var tab = _ref2.tab,
          name = _ref2.name;

      _this.setState({
        tabActive: tab
      });
      _this.props.onClick(tab, name);
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Tab, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var tabActive = this.state.tabActive;
      var _props = this.props,
          testID = _props.testID,
          className = _props.className,
          centered = _props.centered,
          right = _props.right,
          fullwidth = _props.fullwidth,
          fullwidthEqualSized = _props.fullwidthEqualSized,
          children = _props.children;


      var classes = (0, _classnames2.default)('tabs', {
        'is-centered': centered,
        'is-right': right,
        'is-fullwidth': fullwidth || fullwidthEqualSized,
        equal: fullwidthEqualSized
      });
      return _react2.default.createElement(
        'div',
        { className: 'box-tab ' + className },
        _react2.default.createElement(
          'div',
          { className: classes },
          _react2.default.createElement(
            'ul',
            null,
            _react2.default.Children.map(children, function (child, index) {
              return _react2.default.createElement(_TabItem2.default, {
                testID: testID,
                name: child.props.name,
                title: child.props.title,
                icon: child.props.icon,
                active: tabActive === index + 1,
                index: index,
                onClick: _this2.onClickTab
              });
            })
          )
        ),
        _react2.default.Children.map(children, function (child, index) {
          var isActive = tabActive === index + 1;
          if (isActive && child.props.children) {
            return _react2.default.createElement(
              _TabContent2.default,
              null,
              child.props.children
            );
          }
          return null;
        })
      );
    }
  }]);

  return Tab;
}(_react.PureComponent), _class.propTypes = {
  testID: _propTypes2.default.string,
  className: _propTypes2.default.string,
  defaultActive: _propTypes2.default.number,
  centered: _propTypes2.default.bool,
  right: _propTypes2.default.bool,
  fullwidth: _propTypes2.default.bool,
  fullwidthEqualSized: _propTypes2.default.bool,
  /**
   * Return (tab, tabName)
   */
  onClick: _propTypes2.default.func
}, _class.defaultProps = {
  testID: 'tab',
  defaultActive: 1,
  onClick: function onClick(tab, tabName) {
    return null;
  }
}, _temp2);


Tab.Item = _TabItem2.default;

exports.default = Tab;