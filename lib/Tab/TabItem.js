'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TabItem = (_temp = _class = function (_PureComponent) {
  _inherits(TabItem, _PureComponent);

  function TabItem() {
    _classCallCheck(this, TabItem);

    return _possibleConstructorReturn(this, (TabItem.__proto__ || Object.getPrototypeOf(TabItem)).apply(this, arguments));
  }

  _createClass(TabItem, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          testID = _props.testID,
          name = _props.name,
          title = _props.title,
          icon = _props.icon,
          active = _props.active,
          index = _props.index,
          _onClick = _props.onClick;

      var classes = (0, _classnames2.default)('', {
        'is-active': active
      });

      return _react2.default.createElement(
        'li',
        {
          'data-test-id': testID + '-' + index,
          className: classes,
          onClick: function onClick() {
            return _onClick({ tab: index + 1, name: name });
          }
        },
        _react2.default.createElement(
          'a',
          null,
          icon && _react2.default.createElement(
            'span',
            { className: 'icon is-small' },
            _react2.default.createElement('i', { className: icon })
          ),
          title && _react2.default.createElement(
            'span',
            null,
            title
          )
        )
      );
    }
  }]);

  return TabItem;
}(_react.PureComponent), _class.propTypes = {
  testID: _propTypes2.default.string,
  name: _propTypes2.default.string,
  title: _propTypes2.default.string,
  icon: _propTypes2.default.string,
  active: _propTypes2.default.bool,
  index: _propTypes2.default.number,
  onClick: _propTypes2.default.func
}, _temp);
exports.default = TabItem;