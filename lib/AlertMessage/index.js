'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp2;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactDom = require('react-dom');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AlertMessage = (_temp2 = _class = function (_Component) {
  _inherits(AlertMessage, _Component);

  function AlertMessage() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, AlertMessage);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = AlertMessage.__proto__ || Object.getPrototypeOf(AlertMessage)).call.apply(_ref, [this].concat(args))), _this), _this.componentDidMount = function () {
      var closeTime = _this.props.closeTime;

      if (closeTime) {
        setTimeout(function () {
          return closeAlertMessage();
        }, 1000 * closeTime);
      }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(AlertMessage, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          testID = _props.testID,
          type = _props.type,
          message = _props.message;

      return _react2.default.createElement(
        'div',
        { className: type },
        _react2.default.createElement(
          'div',
          { className: 'box-alert-message animated fadeInDown' },
          _react2.default.createElement(
            'a',
            {
              'data-test-id': testID + '-close-button',
              onClick: function onClick() {
                return closeAlertMessage();
              },
              className: 'close'
            },
            _react2.default.createElement('i', { className: 'fa fa-times-circle', 'aria-hidden': 'true' })
          ),
          _react2.default.createElement(
            'div',
            { className: 'detail text-center' },
            message
          )
        )
      );
    }
  }]);

  return AlertMessage;
}(_react.Component), _class.propTypes = {
  testID: _propTypes2.default.string,
  type: _propTypes2.default.string.isRequired,
  message: _propTypes2.default.string,
  closeTime: _propTypes2.default.number
}, _class.defaultProps = {
  testID: 'alert-message'
}, _temp2);


function createElemetReactAlert(properties) {
  var divTarget = document.createElement('div');
  divTarget.id = 'box-react-alert-message';
  document.body.appendChild(divTarget);

  (0, _reactDom.render)(_react2.default.createElement(AlertMessage, properties), divTarget);
}

function openAlertMessage(properties) {
  closeAlertMessage().then(function () {
    createElemetReactAlert(properties);
  });
}

function closeAlertMessage() {
  return new Promise(function (resolve) {
    var target = document.getElementById('box-react-alert-message');
    if (!target) resolve();

    target.classList.add('alert-fade-out');

    var remove = function remove() {
      target.removeEventListener('webkitAnimationEnd', remove, false);
      target.removeEventListener('animationend', remove, false);
      target.addEventListener('onclick', remove, false);
      target.parentNode.removeChild(target);
      resolve();
    };

    target.addEventListener('webkitAnimationEnd', remove, false);
    target.addEventListener('animationend', remove, false);
    target.addEventListener('onclick', remove, false);
  });
}

exports.default = {
  open: openAlertMessage,
  close: closeAlertMessage
};