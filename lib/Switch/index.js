'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp2;
// import classNames from 'classnames'


var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Switch = (_temp2 = _class = function (_PureComponent) {
  _inherits(Switch, _PureComponent);

  function Switch() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Switch);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Switch.__proto__ || Object.getPrototypeOf(Switch)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      isChecked: _this.props.checked
    }, _this.handleCheckboxChange = function () {
      var onToggle = _this.props.onToggle;

      _this.setState(function (prevState) {
        return { isChecked: !prevState.isChecked };
      });
      onToggle(!_this.state.isChecked);
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Switch, [{
    key: 'render',
    value: function render() {
      var isChecked = this.state.isChecked;
      var _props = this.props,
          testID = _props.testID,
          className = _props.className,
          label = _props.label,
          onlyContain = _props.onlyContain,
          disabled = _props.disabled;


      var switchItem = _react2.default.createElement(
        'div',
        { className: 'switch' },
        _react2.default.createElement('input', {
          'data-test-id': testID,
          type: 'checkbox',
          className: 'cbx',
          checked: isChecked,
          readOnly: 'readOnly',
          disabled: disabled,
          onClick: this.handleCheckboxChange
        }),
        _react2.default.createElement('label', { className: 'ui-switch' })
      );

      if (onlyContain) return switchItem;

      return _react2.default.createElement(
        'div',
        { className: 'field ' + className },
        label && _react2.default.createElement(
          'label',
          { className: 'label' },
          label
        ),
        switchItem
      );
    }
  }]);

  return Switch;
}(_react.PureComponent), _class.propTypes = {
  testID: _propTypes2.default.string,
  className: _propTypes2.default.string,
  onlyContain: _propTypes2.default.bool,
  label: _propTypes2.default.string,
  checked: _propTypes2.default.bool,
  disabled: _propTypes2.default.bool,
  /**
   * Return (isChecked)
   */
  onToggle: _propTypes2.default.func
}, _class.defaultProps = {
  testID: 'switch',
  label: 'checkbox',
  checked: false,
  onToggle: function onToggle(isChecked) {
    return null;
  }
}, _temp2);
exports.default = Switch;