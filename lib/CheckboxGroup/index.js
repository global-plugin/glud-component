'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp2;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Checkbox = require('../Checkbox');

var _Checkbox2 = _interopRequireDefault(_Checkbox);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CheckboxGroup = (_temp2 = _class = function (_Component) {
  _inherits(CheckboxGroup, _Component);

  function CheckboxGroup() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, CheckboxGroup);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = CheckboxGroup.__proto__ || Object.getPrototypeOf(CheckboxGroup)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      values: []
    }, _this.componentDidMount = function () {
      var _this$props = _this.props,
          options = _this$props.options,
          value = _this$props.value;


      _this.setState(function () {
        return {
          values: _this.getOptionIsChecked(options, value)
        };
      });
    }, _this.componentWillReceiveProps = function (nextProps) {
      var options = nextProps.options,
          value = nextProps.value;

      _this.setState(function () {
        return {
          values: _this.getOptionIsChecked(options, value)
        };
      });
    }, _this.isOptionChecked = function (value, currentValue) {
      if (value.value) return value.value === currentValue;
      return value === currentValue;
    }, _this.isChecked = function (currentValue) {
      var values = _this.state.values;

      return values.some(function (item) {
        return _this.isOptionChecked(item, currentValue);
      });
    }, _this.getOptionIsChecked = function (options, value) {
      return options.filter(function (item) {
        return value.some(function (v) {
          return v === item.value || v.value === item.value;
        });
      });
    }, _this.handleCheckboxChange = function (option, isChecked) {
      var values = _this.state.values;

      if (isChecked) {
        var _nextValues = [].concat(_toConsumableArray(values), [option]);
        _this.updateValues(_nextValues);
        return true;
      }

      var nextValues = values.filter(function (item) {
        return item.value !== option.value;
      });
      _this.updateValues(nextValues);
    }, _this.updateValues = function (nextValues) {
      _this.setState(function () {
        return {
          values: nextValues
        };
      });
      _this.props.onChange(nextValues);
    }, _this.renderCheckbox = function (option) {
      return _react2.default.createElement(_Checkbox2.default, {
        testID: option.testID || option.name,
        name: option.name,
        key: option.value,
        label: option.label,
        value: option.value,
        disabled: option.disabled,
        checked: _this.isChecked(option.value),
        onChange: function onChange(isChecked) {
          return _this.handleCheckboxChange(option, isChecked);
        }
      });
    }, _this.renderCheckboxOptions = function () {
      var _this$props2 = _this.props,
          inline = _this$props2.inline,
          options = _this$props2.options;


      if (inline) return options.map(function (option) {
        return _this.renderCheckbox(option);
      });

      return options.map(function (option, i) {
        return _react2.default.createElement(
          'div',
          { key: i, className: 'box-field' },
          _this.renderCheckbox(option)
        );
      });
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(CheckboxGroup, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          className = _props.className,
          labelGroup = _props.labelGroup,
          isRequired = _props.isRequired;

      return _react2.default.createElement(
        'div',
        { className: 'field ' + className },
        labelGroup && _react2.default.createElement(
          'label',
          { className: 'label' },
          labelGroup,
          ' ',
          isRequired && _react2.default.createElement('i', { className: 'required fas fa-asterisk' })
        ),
        this.renderCheckboxOptions()
      );
    }
  }]);

  return CheckboxGroup;
}(_react.Component), _class.propTypes = {
  className: _propTypes2.default.string,
  labelGroup: _propTypes2.default.string,
  isRequired: _propTypes2.default.bool,
  inline: _propTypes2.default.bool,
  options: _propTypes2.default.array.isRequired,
  value: _propTypes2.default.array.isRequired,
  onChange: _propTypes2.default.func
}, _class.defaultProps = {
  inline: false,
  options: [],
  value: [],
  onChange: function onChange(values) {
    return null;
  }
}, _temp2);
exports.default = CheckboxGroup;