'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Item = require('./Item');

var _Item2 = _interopRequireDefault(_Item);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var InputWithAddons = (_temp = _class = function (_Component) {
  _inherits(InputWithAddons, _Component);

  function InputWithAddons() {
    _classCallCheck(this, InputWithAddons);

    return _possibleConstructorReturn(this, (InputWithAddons.__proto__ || Object.getPrototypeOf(InputWithAddons)).apply(this, arguments));
  }

  _createClass(InputWithAddons, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          className = _props.className,
          centered = _props.centered,
          right = _props.right,
          hasLabelAndMessage = _props.hasLabelAndMessage;

      var classes = (0, _classnames2.default)('field has-addons', {
        'has-label-and-message': hasLabelAndMessage,
        'has-addons-centered': centered,
        'has-addons-right': right
      }, className);

      return _react2.default.createElement(
        'div',
        { className: classes },
        this.props.children
      );
    }
  }]);

  return InputWithAddons;
}(_react.Component), _class.propTypes = {
  centered: _propTypes2.default.bool,
  right: _propTypes2.default.bool,
  hasLabelAndMessage: _propTypes2.default.bool,
  className: _propTypes2.default.string
}, _temp);


InputWithAddons.Item = _Item2.default;

exports.default = InputWithAddons;