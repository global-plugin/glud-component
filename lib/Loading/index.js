'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactDom = require('react-dom');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Loading = (_temp = _class = function (_Component) {
  _inherits(Loading, _Component);

  function Loading() {
    _classCallCheck(this, Loading);

    return _possibleConstructorReturn(this, (Loading.__proto__ || Object.getPrototypeOf(Loading)).apply(this, arguments));
  }

  _createClass(Loading, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          className = _props.className,
          label = _props.label,
          component = _props.component;

      return _react2.default.createElement(
        'div',
        { className: 'box-loading open ' + className },
        _react2.default.createElement(
          'div',
          { className: 'loading' },
          _react2.default.createElement(
            'div',
            null,
            label
          ),
          _react2.default.createElement('br', null),
          component
        )
      );
    }
  }]);

  return Loading;
}(_react.Component), _class.propTypes = {
  className: _propTypes2.default.string,
  label: _propTypes2.default.string,
  component: _propTypes2.default.node
}, _temp);


function createElemetReactLoading(properties) {
  var divTarget = document.createElement('div');
  divTarget.id = 'box-react-loading';
  document.body.appendChild(divTarget);

  (0, _reactDom.render)(_react2.default.createElement(Loading, properties), divTarget);
}

function openLoading(properties) {
  closeLoading().then(function () {
    createElemetReactLoading(properties);
  });
}

function closeLoading() {
  return new Promise(function (resolve) {
    var target = document.getElementById('box-react-loading');
    if (!target) resolve();

    target.classList.add('loading-fade-out');

    var remove = function remove() {
      // target.removeEventListener('webkitAnimationEnd', remove, false)
      // target.removeEventListener('animationend', remove, false)
      target.parentNode.removeChild(target);
      var targetOther = document.getElementById('box-react-loading');
      if (!targetOther) resolve();
      closeLoading();
    };

    remove();

    // target.addEventListener('webkitAnimationEnd', remove, false)
    // target.addEventListener('onclick', remove, false)
  });
}

exports.default = {
  open: openLoading,
  close: closeLoading
};