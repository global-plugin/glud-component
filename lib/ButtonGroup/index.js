'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ButtonGroup = (_temp = _class = function (_React$PureComponent) {
  _inherits(ButtonGroup, _React$PureComponent);

  function ButtonGroup() {
    _classCallCheck(this, ButtonGroup);

    return _possibleConstructorReturn(this, (ButtonGroup.__proto__ || Object.getPrototypeOf(ButtonGroup)).apply(this, arguments));
  }

  _createClass(ButtonGroup, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          className = _props.className,
          hasAddons = _props.hasAddons,
          centered = _props.centered,
          right = _props.right,
          fullwidth = _props.fullwidth,
          fullwidthEqualSized = _props.fullwidthEqualSized,
          children = _props.children;


      var classes = (0, _classnames2.default)('buttons', {
        'has-addons': hasAddons,
        'is-centered': centered,
        'is-right': right,
        fullwidth: fullwidth || fullwidthEqualSized,
        equal: fullwidthEqualSized,
        className: className
      });

      return _react2.default.createElement(
        'div',
        { className: classes },
        children
      );
    }
  }]);

  return ButtonGroup;
}(_react2.default.PureComponent), _class.propTypes = {
  className: _propTypes2.default.string,
  hasAddons: _propTypes2.default.bool,
  centered: _propTypes2.default.bool,
  right: _propTypes2.default.bool,
  fullwidth: _propTypes2.default.bool,
  fullwidthEqualSized: _propTypes2.default.bool
}, _temp);
exports.default = ButtonGroup;