'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _moment2 = require('moment');

var _moment3 = _interopRequireDefault(_moment2);

require('moment-range');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function isMomentRange(val) {
  return val && val.start && val.end && _moment3.default.isMoment(val.start) && _moment3.default.isMoment(val.end);
}

exports.default = {
  momentOrMomentRange: function momentOrMomentRange(props, propName) {
    var val = props[propName];

    if (!val) {
      return null;
    } else if (_moment3.default.isMoment(val)) {
      return null;
    } else if (isMomentRange(val)) {
      return null;
    }
    return new Error('\'' + propName + '\' must be a moment or a moment range');
  },
  moment: function moment(props, propName) {
    var val = props[propName];

    if (!val) {
      return null;
    } else if (_moment3.default.isMoment(val)) {
      return null;
    }
    return new Error('\'' + propName + '\' must be a moment');
  },
  momentRange: function momentRange(props, propName) {
    var val = props[propName];

    if (!val) {
      return null;
    } else if (isMomentRange(val)) {
      return null;
    }
    return new Error('\'' + propName + '\' must be a moment range');
  }
};