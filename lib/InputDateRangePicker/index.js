'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp2;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactDaterangePicker = require('./react-daterange-picker');

var _reactDaterangePicker2 = _interopRequireDefault(_reactDaterangePicker);

var _get = require('lodash/get');

var _get2 = _interopRequireDefault(_get);

var _Input = require('../Input');

var _Input2 = _interopRequireDefault(_Input);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _momentRange = require('moment-range');

var _customTyepsUtil = require('./customTyeps-util');

var _customTyepsUtil2 = _interopRequireDefault(_customTyepsUtil);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var moment = (0, _momentRange.extendMoment)(_moment2.default);

var InputDateRangePicker = (_temp2 = _class = function (_Component) {
  _inherits(InputDateRangePicker, _Component);

  function InputDateRangePicker() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, InputDateRangePicker);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = InputDateRangePicker.__proto__ || Object.getPrototypeOf(InputDateRangePicker)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      isShowCalendar: false,
      dateRangeValue: null
    }, _this.componentDidMount = function () {
      _this.setState({
        dateRangeValue: _this.props.value
      });
    }, _this.componentWillReceiveProps = function (nextProps) {
      _this.setState({
        dateRangeValue: nextProps.value
      });
    }, _this.handleInputFocus = function (e) {
      _this.onShowDatePicker();
      _this.props.onFocus(e);
      _this.input.blur();
    }, _this.handleClickCloseDatePicker = function () {
      _this.onCloseDatePicker();
    }, _this.handSelectDate = function (dateRangeValue) {
      _this.setState({
        dateRangeValue: dateRangeValue
      });
      _this.props.onChange(dateRangeValue);
      _this.onCloseDatePicker();
    }, _this.onShowDatePicker = function () {
      _this.setState(function (prevState) {
        return {
          isShowCalendar: true
        };
      });
    }, _this.onCloseDatePicker = function () {
      _this.setState(function (prevState) {
        return {
          isShowCalendar: false
        };
      });
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(InputDateRangePicker, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _state = this.state,
          isShowCalendar = _state.isShowCalendar,
          dateRangeValue = _state.dateRangeValue;
      var _props = this.props,
          testID = _props.testID,
          onlyContain = _props.onlyContain,
          label = _props.label,
          name = _props.name,
          isRequired = _props.isRequired,
          isSuccess = _props.isSuccess,
          isError = _props.isError,
          message = _props.message,
          format = _props.format,
          disabled = _props.disabled,
          minimumDate = _props.minimumDate,
          maximumDate = _props.maximumDate;


      var startDate = (0, _get2.default)(dateRangeValue, 'start', null);
      var endDate = (0, _get2.default)(dateRangeValue, 'end', null);
      var value = dateRangeValue ? startDate.format(format) + ' - ' + endDate.format(format) : '';

      return _react2.default.createElement(
        _react.Fragment,
        null,
        _react2.default.createElement(_Input2.default, {
          testID: testID,
          ref: function ref(input) {
            return _this2.input = input;
          },
          onlyContain: onlyContain,
          label: label,
          name: name,
          isRequired: isRequired,
          isSuccess: isSuccess,
          isError: isError,
          disabled: disabled,
          message: message,
          onFocus: this.handleInputFocus,
          value: value,
          iconRight: function iconRight() {
            return _react2.default.createElement('i', { className: 'fas fa-calendar' });
          }
        }),
        isShowCalendar && _react2.default.createElement(
          'div',
          { className: 'box-datarange-wrapper show' },
          _react2.default.createElement('a', {
            'data-test-id': testID + '-close-button',
            className: 'delete',
            onClick: this.handleClickCloseDatePicker
          }),
          _react2.default.createElement(_reactDaterangePicker2.default, {
            numberOfCalendars: 2,
            selectionType: 'range',
            singleDateRange: true,
            initialFromValue: true,
            minimumDate: minimumDate,
            maximumDate: maximumDate,
            value: dateRangeValue,
            onSelect: this.handSelectDate
          })
        )
      );
    }
  }]);

  return InputDateRangePicker;
}(_react.Component), _class.propTypes = {
  testID: _propTypes2.default.string,
  onlyContain: _propTypes2.default.bool,
  label: _propTypes2.default.string,
  name: _propTypes2.default.string,
  isRequired: _propTypes2.default.bool,
  isSuccess: _propTypes2.default.bool,
  isError: _propTypes2.default.bool,
  message: _propTypes2.default.string,
  disabled: _propTypes2.default.bool,
  format: _propTypes2.default.string,
  value: _propTypes2.default.oneOfType([_customTyepsUtil2.default.momentRange, _propTypes2.default.oneOf([null])]),
  onChange: _propTypes2.default.func,
  onFocus: _propTypes2.default.func,
  maximumDate: _propTypes2.default.instanceOf(Date),
  minimumDate: _propTypes2.default.instanceOf(Date)
}, _class.defaultProps = {
  testID: 'input-date-picker-range',
  format: 'DD/MM/YYYY',
  onChange: function onChange() {
    return null;
  },
  onFocus: function onFocus() {
    return null;
  },
  value: moment.range(moment().startOf('days'), moment().add(1, 'days'))
}, _temp2);
exports.default = InputDateRangePicker;