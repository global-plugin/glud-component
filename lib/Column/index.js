'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Column = (_temp = _class = function (_Component) {
  _inherits(Column, _Component);

  function Column() {
    _classCallCheck(this, Column);

    return _possibleConstructorReturn(this, (Column.__proto__ || Object.getPrototypeOf(Column)).apply(this, arguments));
  }

  _createClass(Column, [{
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          D = _props.D,
          L = _props.L,
          T = _props.T,
          M = _props.M,
          SM = _props.SM,
          XSM = _props.XSM,
          orderD = _props.orderD,
          orderL = _props.orderL,
          orderT = _props.orderT,
          orderM = _props.orderM,
          orderSM = _props.orderSM,
          orderXSM = _props.orderXSM,
          top = _props.top,
          middle = _props.middle,
          bottom = _props.bottom,
          className = _props.className,
          children = _props.children;


      var classes = (0, _classnames2.default)('', (_classNames = {}, _defineProperty(_classNames, className, className), _defineProperty(_classNames, 'D-' + D, D), _defineProperty(_classNames, 'L-' + L, L), _defineProperty(_classNames, 'T-' + T, T), _defineProperty(_classNames, 'M-' + M, M), _defineProperty(_classNames, 'SM-' + SM, SM), _defineProperty(_classNames, 'XSM-' + XSM, XSM), _defineProperty(_classNames, 'D-order-' + orderD, orderD), _defineProperty(_classNames, 'L-order-' + orderL, orderL), _defineProperty(_classNames, 'T-order-' + orderT, orderT), _defineProperty(_classNames, 'M-order-' + orderM, orderM), _defineProperty(_classNames, 'SM-order-' + orderSM, orderSM), _defineProperty(_classNames, 'XSM-order-' + orderXSM, orderXSM), _defineProperty(_classNames, 'vertical-top', top), _defineProperty(_classNames, 'vertical-middle', middle), _defineProperty(_classNames, 'vertical-bottom', bottom), _classNames));
      return _react2.default.createElement(
        'div',
        { className: classes },
        children
      );
    }
  }]);

  return Column;
}(_react.Component), _class.propTypes = {
  /**
   * Desktop
   */
  D: _propTypes2.default.number,
  /**
   * Laptop
   */
  L: _propTypes2.default.number,
  /**
   * Tablet
   */
  T: _propTypes2.default.number,
  /**
   * Mobile
   */
  M: _propTypes2.default.number,
  /**
   * Small Mobile
   */
  SM: _propTypes2.default.number,
  /**
   * Extra Small Mobile
   */
  XSM: _propTypes2.default.number,
  orderD: _propTypes2.default.number,
  orderL: _propTypes2.default.number,
  orderT: _propTypes2.default.number,
  orderM: _propTypes2.default.number,
  orderSM: _propTypes2.default.number,
  orderXSM: _propTypes2.default.number,
  top: _propTypes2.default.bool,
  middle: _propTypes2.default.bool,
  bottom: _propTypes2.default.bool,
  className: _propTypes2.default.string
}, _temp);
exports.default = Column;