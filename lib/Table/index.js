'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.getDataByPage = exports.getPagination = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp2, _class2, _temp3;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Row = require('../Row');

var _Row2 = _interopRequireDefault(_Row);

var _Column = require('../Column');

var _Column2 = _interopRequireDefault(_Column);

var _Select = require('../Select');

var _Select2 = _interopRequireDefault(_Select);

var _SmartSearch = require('./SmartSearch');

var _SmartSearch2 = _interopRequireDefault(_SmartSearch);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var getPagination = exports.getPagination = function getPagination(totalRecord) {
  var viewPerPage = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 20;

  var pageTotal = Math.ceil(totalRecord / viewPerPage);
  var value = [];
  var _from = 1;
  var _to = viewPerPage;

  for (var i = 1; i <= pageTotal; i++) {
    if (i > 1) {
      _from += viewPerPage;
      _to += viewPerPage;
    }
    value.push({
      pageNumber: i,
      from: _from,
      to: _to
    });
  }
  return value;
};

var getDataByPage = exports.getDataByPage = function getDataByPage(listData, pagination, currentPage) {
  var currentView = pagination.filter(function (item) {
    return Number(currentPage) === item.pageNumber;
  })[0];
  var result = [];
  if (listData.length > 0) {
    for (var i = 1; i <= listData.length; i++) {
      if (i >= currentView.from && i <= currentView.to) {
        result.push(listData[i - 1]);
      }
    }
  }

  return result;
};

var getTotalRecord = function getTotalRecord(totalDataLength, totalRecord) {
  return totalRecord || totalDataLength;
};

var Table = (_temp2 = _class = function (_PureComponent) {
  _inherits(Table, _PureComponent);

  function Table() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Table);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Table.__proto__ || Object.getPrototypeOf(Table)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      paginations: getPagination(getTotalRecord(_this.props.dataList.length, _this.props.totalRecord), _this.props.viewPerPage),
      currentPage: 1,
      dataFromSearch: undefined
    }, _this.handleCurrentPage = function (nextProps) {
      if (nextProps.onChangePage && nextProps.dataList.length > 0) return false;
      if (nextProps.dataList.length !== _this.props.dataList.length) return true;
      return false;
    }, _this.search = function (dataFromSearch) {
      _this.setState(function (prevState, props) {
        var total = props.totalRecord || props.dataList.length;
        if (dataFromSearch) total = dataFromSearch.length;

        return {
          paginations: getPagination(total, props.viewPerPage),
          dataFromSearch: dataFromSearch,
          currentPage: 1
        };
      });
    }, _this.onChangePage = function (page) {
      _this.setState(function () {
        return {
          currentPage: page
        };
      });

      if (_this.props.onChangePage) {
        var currentView = _this.state.paginations.filter(function (item) {
          return Number(page) === item.pageNumber;
        })[0];
        _this.props.onChangePage(currentView);
      }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Table, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      var _this2 = this;

      this.setState(function (prevState) {
        return {
          paginations: getPagination(getTotalRecord(nextProps.dataList.length, nextProps.totalRecord), nextProps.viewPerPage),
          currentPage: _this2.handleCurrentPage(nextProps) ? 1 : prevState.currentPage
        };
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _state = this.state,
          paginations = _state.paginations,
          currentPage = _state.currentPage,
          dataFromSearch = _state.dataFromSearch;
      var _props = this.props,
          className = _props.className,
          dataList = _props.dataList,
          elementHead = _props.elementHead,
          elementBody = _props.elementBody,
          elementTD = _props.elementTD,
          pagination = _props.pagination,
          totalRecord = _props.totalRecord,
          onChangePage = _props.onChangePage,
          fieldsForSearch = _props.fieldsForSearch,
          placeholderSearch = _props.placeholderSearch;


      var datas = dataFromSearch || dataList;
      var tableRows = pagination && !onChangePage ? getDataByPage(datas, paginations, currentPage) : datas;

      var ttr = dataFromSearch ? dataFromSearch.length : totalRecord;

      var isShowPaginationTop = pagination && /(top|both)/.test(pagination);
      var isShowPaginationBottom = pagination && /(bottom|both)/.test(pagination);

      return _react2.default.createElement(
        'div',
        { className: className },
        fieldsForSearch && _react2.default.createElement(
          _Row2.default,
          null,
          _react2.default.createElement(
            _Column2.default,
            { D: 6 },
            _react2.default.createElement(_SmartSearch2.default, {
              dataList: dataList,
              fieldsForSearch: fieldsForSearch,
              placeholder: placeholderSearch,
              onKeySearch: this.search
            }),
            _react2.default.createElement('br', null)
          )
        ),
        isShowPaginationTop && _react2.default.createElement(
          'div',
          null,
          _react2.default.createElement(PaginationGroup, {
            pagination: paginations,
            totalRecord: totalRecord,
            currentPage: currentPage,
            onChangePage: this.onChangePage
          }),
          _react2.default.createElement('br', null)
        ),
        _react2.default.createElement(
          'div',
          { className: 'table-responsive' },
          _react2.default.createElement(
            'table',
            { className: 'table is-bordered is-striped is-hoverable is-fullwidth' },
            _react2.default.createElement(
              'thead',
              null,
              elementHead()
            ),
            elementBody ? elementBody(tableRows) : _react2.default.createElement(
              'tbody',
              null,
              tableRows.map(function (data, i) {
                return elementTD(data, i);
              })
            )
          )
        ),
        isShowPaginationBottom && _react2.default.createElement(PaginationGroup, {
          pagination: paginations,
          totalRecord: ttr,
          currentPage: currentPage,
          onChangePage: this.onChangePage
        })
      );
    }
  }]);

  return Table;
}(_react.PureComponent), _class.propTypes = {
  className: _propTypes2.default.string,
  dataList: _propTypes2.default.array,
  elementHead: _propTypes2.default.func,
  elementBody: _propTypes2.default.func,
  elementTD: _propTypes2.default.func,
  onChangePage: _propTypes2.default.func,
  placeholderSearch: _propTypes2.default.string,
  fieldsForSearch: _propTypes2.default.object,
  /**
   * top | bottom | both
   */
  pagination: _propTypes2.default.string,
  viewPerPage: _propTypes2.default.number,
  totalRecord: _propTypes2.default.number
}, _class.defaultProps = {
  totalRecord: 0,
  viewPerPage: 20,
  dataList: [],
  elementHead: function elementHead() {
    return null;
  },
  elementTD: function elementTD() {
    return null;
  }
}, _temp2);
exports.default = Table;
var PaginationGroup = (_temp3 = _class2 = function (_Component) {
  _inherits(PaginationGroup, _Component);

  function PaginationGroup() {
    _classCallCheck(this, PaginationGroup);

    return _possibleConstructorReturn(this, (PaginationGroup.__proto__ || Object.getPrototypeOf(PaginationGroup)).apply(this, arguments));
  }

  _createClass(PaginationGroup, [{
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          totalRecord = _props2.totalRecord,
          currentPage = _props2.currentPage,
          _props2$pagination = _props2.pagination,
          pagination = _props2$pagination === undefined ? [] : _props2$pagination,
          onChangePage = _props2.onChangePage;


      return _react2.default.createElement(
        _Row2.default,
        { justified: true },
        _react2.default.createElement(
          _Column2.default,
          { D: 6, middle: true },
          _react2.default.createElement(
            'b',
            null,
            '\u0E08\u0E33\u0E19\u0E27\u0E19\u0E17\u0E31\u0E49\u0E07\u0E2B\u0E21\u0E14:'
          ),
          ' ',
          totalRecord,
          ' \u0E23\u0E32\u0E22\u0E01\u0E32\u0E23'
        ),
        _react2.default.createElement(
          _Column2.default,
          { D: 3, middle: true },
          _react2.default.createElement(
            'div',
            { className: 'field has-addons is-pulled-right' },
            _react2.default.createElement(_Select2.default, {
              inline: true,
              onlyContain: true,
              value: currentPage,
              onChange: function onChange(e) {
                return onChangePage(e.target.value);
              },
              options: pagination.map(function (item) {
                return {
                  label: item.pageNumber,
                  value: item.pageNumber
                };
              })
            }),
            _react2.default.createElement(
              'p',
              { className: 'control' },
              _react2.default.createElement(
                'a',
                { className: 'button is-static' },
                'of ',
                pagination.length,
                ' Pages'
              )
            )
          )
        )
      );
    }
  }]);

  return PaginationGroup;
}(_react.Component), _class2.propTypes = {
  pagination: _propTypes2.default.array,
  totalRecord: _propTypes2.default.number,
  currentPage: _propTypes2.default.oneOfType([_propTypes2.default.number, _propTypes2.default.string]),
  onChangePage: _propTypes2.default.func
}, _temp3);