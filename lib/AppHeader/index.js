'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp2;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AppHeader = (_temp2 = _class = function (_PureComponent) {
  _inherits(AppHeader, _PureComponent);

  function AppHeader() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, AppHeader);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = AppHeader.__proto__ || Object.getPrototypeOf(AppHeader)).call.apply(_ref, [this].concat(args))), _this), _this.navLeftRenderer = function () {
      var _this$props = _this.props,
          leftDisable = _this$props.leftDisable,
          onClickLeft = _this$props.onClickLeft,
          leftIcon = _this$props.leftIcon,
          leftActions = _this$props.leftActions,
          testID = _this$props.testID;


      if (leftDisable) return null;

      var shouldRenderBox = function shouldRenderBox() {
        if (!leftIcon && leftActions.length === 0) return true;
        return false;
      };

      if (shouldRenderBox()) return _react2.default.createElement('div', { className: 'nav-left' });

      if (leftActions.length > 0) {
        return leftActions.map(function (nav, i) {
          return _react2.default.createElement(
            'div',
            {
              key: i,
              className: 'nav-left ' + (nav.disabled && 'disabled'),
              'data-test-id': testID + '-nav-left-' + i,
              onClick: function onClick() {
                return !nav.disabled && nav.onClick();
              }
            },
            !nav.disabled && _react2.default.createElement('i', { className: nav.icon })
          );
        });
      }

      return _react2.default.createElement(
        'div',
        {
          className: 'nav-left',
          'data-test-id': testID + '-nav-left',
          onClick: onClickLeft
        },
        leftIcon && _react2.default.createElement('i', { className: leftIcon })
      );
    }, _this.navRightRenderer = function () {
      var _this$props2 = _this.props,
          rightDisable = _this$props2.rightDisable,
          onClickRight = _this$props2.onClickRight,
          rightIcon = _this$props2.rightIcon,
          rightActions = _this$props2.rightActions,
          testID = _this$props2.testID;


      if (rightDisable) return null;

      var shouldRenderBox = function shouldRenderBox() {
        if (!rightIcon && rightActions.length === 0) return true;
        return false;
      };

      if (shouldRenderBox()) return _react2.default.createElement('div', { className: 'nav-right' });

      if (rightActions.length > 0) {
        return rightActions.map(function (nav, i) {
          return _react2.default.createElement(
            'div',
            {
              key: i,
              className: 'nav-right ' + (nav.disabled && 'disabled'),
              'data-test-id': testID + '-nav-right-' + i,
              onClick: function onClick() {
                return !nav.disabled && nav.onClick();
              }
            },
            !nav.disabled && _react2.default.createElement('i', { className: nav.icon })
          );
        });
      }

      return _react2.default.createElement(
        'div',
        {
          className: 'nav-right',
          'data-test-id': testID + '-nav-right',
          onClick: onClickRight
        },
        rightIcon && _react2.default.createElement('i', { className: rightIcon })
      );
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(AppHeader, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          className = _props.className,
          title = _props.title,
          subTitle = _props.subTitle,
          titleLeft = _props.titleLeft;


      return _react2.default.createElement(
        'header',
        { className: 'app-header ' + className },
        this.navLeftRenderer(),
        _react2.default.createElement(
          'div',
          { className: 'nav-title ' + (titleLeft && 'left') },
          _react2.default.createElement(
            'div',
            { className: 'app-title' },
            title
          ),
          _react2.default.createElement(
            'span',
            null,
            subTitle
          )
        ),
        this.navRightRenderer()
      );
    }
  }]);

  return AppHeader;
}(_react.PureComponent), _class.propTypes = {
  testID: _propTypes2.default.string,
  className: _propTypes2.default.string,
  rightDisable: _propTypes2.default.bool,
  leftDisable: _propTypes2.default.bool,
  rightIcon: _propTypes2.default.string,
  leftIcon: _propTypes2.default.string,
  onClickRight: _propTypes2.default.func,
  onClickLeft: _propTypes2.default.func,
  rightActions: _propTypes2.default.shape({
    disabled: _propTypes2.default.bool,
    icon: _propTypes2.default.string.isRequired,
    onClick: _propTypes2.default.func.isRequired
  }),
  title: _propTypes2.default.string,
  titleLeft: _propTypes2.default.string,
  subTitle: _propTypes2.default.string
}, _class.defaultProps = {
  onClickLeft: function onClickLeft() {
    return console.log('Top Navigation Left action cliked');
  },
  onClickRight: function onClickRight() {
    return console.log('Top Navigation right action cliked');
  },
  leftActions: [],
  rightActions: [],
  testID: 'app-header'
}, _temp2);
exports.default = AppHeader;