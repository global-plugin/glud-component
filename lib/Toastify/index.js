'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Toastify = function (_React$Component) {
  _inherits(Toastify, _React$Component);

  function Toastify() {
    _classCallCheck(this, Toastify);

    return _possibleConstructorReturn(this, (Toastify.__proto__ || Object.getPrototypeOf(Toastify)).apply(this, arguments));
  }

  _createClass(Toastify, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          type = _props.type,
          message = _props.message,
          component = _props.component;


      var UIComponent = null;
      if (typeof component === 'function') {
        UIComponent = component();
      }

      var classes = (0, _classnames2.default)('toastify', {
        'is-success': type === 'SUCCESS',
        'is-danger': type === 'ERROR',
        'is-warning': type === 'WARNING',
        'is-info': type === 'INFO',
        'is-loading': type === 'LOADING',
        'hide-icon': UIComponent
      });

      return _react2.default.createElement(
        'div',
        { className: classes },
        UIComponent,
        message
      );
    }
  }]);

  return Toastify;
}(_react2.default.Component);

function createToast(_ref) {
  var type = _ref.type,
      _ref$position = _ref.position,
      position = _ref$position === undefined ? 'center' : _ref$position,
      message = _ref.message,
      closeTime = _ref.closeTime,
      component = _ref.component,
      onClose = _ref.onClose;

  var wrapperClassName = 'react-toastify-wrapper ' + position;
  var wrapper = document.getElementsByClassName(wrapperClassName);
  var hasWrapper = wrapper.length === 1;

  var divTarget = document.createElement('div');
  divTarget.className = 'move-up-enter';

  if (hasWrapper) {
    wrapper[0].appendChild(divTarget);
  }

  if (!hasWrapper) {
    var createWrapper = document.createElement('div');
    createWrapper.className = wrapperClassName;
    createWrapper.appendChild(divTarget);
    document.body.appendChild(createWrapper);
  }

  (0, _reactDom.render)(_react2.default.createElement(Toastify, { type: type, message: message, component: component }), divTarget);

  if (closeTime) {
    setTimeout(function () {
      removeToast(divTarget, onClose);
    }, closeTime * 1000);
  }

  return {
    close: function close() {
      return removeToast(divTarget, onClose);
    }
  };
}

function removeToast(target, onClose) {
  target.classList.add('move-up-leave');

  var remove = function remove() {
    target.removeEventListener('webkitAnimationEnd', remove, false);
    target.removeEventListener('animationend', remove, false);
    (0, _reactDom.unmountComponentAtNode)(target);
    target.parentNode.removeChild(target);

    if (typeof onClose === 'function') onClose();
  };

  target.addEventListener('webkitAnimationEnd', remove, false);
  target.addEventListener('animationend', remove, false);
}

exports.default = createToast;