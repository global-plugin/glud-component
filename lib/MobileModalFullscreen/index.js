'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _templateObject = _taggedTemplateLiteral(['\n  label: MobileModalFullscreen;\n\n  .modal-wrapper {\n    width: ', ';\n    border-radius: ', ';\n    overflow: hidden;\n    position: fixed;\n    z-index: 10;\n    top: ', ';\n    bottom: ', ';\n    left: 50%;\n    background: #fff;\n    display: flex;\n    flex-direction: column;\n    transform: translateX(-50%);\n\n    @media screen and (max-width: 700px) {\n      width: 100%;\n      top: 0;\n      bottom: 0;\n      border-radius: 0;\n    }\n  }\n\n  .app-header {\n    flex-shrink: 0;\n  }\n\n  .wrap-content {\n    flex-grow: 1;\n    height: 100%;\n    word-break: break-word;\n    -webkit-overflow-scrolling: touch;\n    overflow-y: ', ';\n  }\n'], ['\n  label: MobileModalFullscreen;\n\n  .modal-wrapper {\n    width: ', ';\n    border-radius: ', ';\n    overflow: hidden;\n    position: fixed;\n    z-index: 10;\n    top: ', ';\n    bottom: ', ';\n    left: 50%;\n    background: #fff;\n    display: flex;\n    flex-direction: column;\n    transform: translateX(-50%);\n\n    @media screen and (max-width: 700px) {\n      width: 100%;\n      top: 0;\n      bottom: 0;\n      border-radius: 0;\n    }\n  }\n\n  .app-header {\n    flex-shrink: 0;\n  }\n\n  .wrap-content {\n    flex-grow: 1;\n    height: 100%;\n    word-break: break-word;\n    -webkit-overflow-scrolling: touch;\n    overflow-y: ', ';\n  }\n']);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _AppHeader = require('../AppHeader');

var _AppHeader2 = _interopRequireDefault(_AppHeader);

var _web = require('react-spring/web.cjs');

var _styled = require('@emotion/styled');

var _styled2 = _interopRequireDefault(_styled);

var _useModalOpenInMobileLayout = require('../useModalOpenInMobileLayout');

var _useModalOpenInMobileLayout2 = _interopRequireDefault(_useModalOpenInMobileLayout);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var MobileModalFullscreen = function MobileModalFullscreen(_ref) {
  var testID = _ref.testID,
      title = _ref.title,
      show = _ref.show,
      leftIcon = _ref.leftIcon,
      onClickLeft = _ref.onClickLeft,
      onClose = _ref.onClose,
      wrap = _ref.wrap,
      headerDisabled = _ref.headerDisabled,
      overflowHidden = _ref.overflowHidden,
      children = _ref.children;

  var transitions = (0, _web.useTransition)(show, null, {
    from: {
      willChange: 'all',
      position: 'fixed',
      zIndex: 10,
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      transform: 'scale(1.03) translateZ(0)',
      backgroundColor: 'hsla(0,0%,4%,.3)',
      opacity: 0
    },
    enter: {
      transform: 'scale(1) translateZ(0)',
      opacity: 1
    },
    leave: {
      transform: 'scale(1.03) translateZ(0)',
      opacity: 0
    },
    config: { mass: 1, tension: 2000, friction: 70 }
  });

  var modalRef = (0, _react.useRef)(null);

  (0, _useModalOpenInMobileLayout2.default)(modalRef, show);

  return _react2.default.createElement(
    _react.Fragment,
    null,
    _react2.default.createElement('div', { ref: modalRef }),
    transitions.map(function (_ref2) {
      var item = _ref2.item,
          key = _ref2.key,
          props = _ref2.props;
      return item && _react2.default.createElement(
        _web.animated.div,
        { key: key, style: props },
        _react2.default.createElement(
          Style,
          { wrap: wrap ? 1 : 0, overflowHidden: overflowHidden },
          _react2.default.createElement(
            'div',
            { className: 'modal-wrapper' },
            !headerDisabled && _react2.default.createElement(_AppHeader2.default, {
              testID: testID,
              title: title,
              leftIcon: leftIcon,
              onClickLeft: onClickLeft,
              rightIcon: 'fas fa-times',
              onClickRight: onClose
            }),
            _react2.default.createElement(
              'div',
              { className: 'wrap-content' },
              children
            )
          )
        )
      );
    })
  );
};

MobileModalFullscreen.propTypes = {
  testID: _propTypes2.default.string,
  title: _propTypes2.default.string.isRequired,
  show: _propTypes2.default.bool.isRequired,
  wrap: _propTypes2.default.bool.isRequired,
  leftIcon: _propTypes2.default.string,
  onClickLeft: _propTypes2.default.func.isRequired,
  onClose: _propTypes2.default.func.isRequired,
  headerDisabled: _propTypes2.default.bool,
  overflowHidden: _propTypes2.default.bool
};

MobileModalFullscreen.defaultProps = {
  testID: 'modal-fullscreen',
  title: 'Modal Title',
  show: false,
  wrap: false,
  onClickLeft: function onClickLeft() {
    return null;
  },
  onClose: function onClose() {
    return null;
  },
  headerDisabled: false,
  overflowHidden: false
};

exports.default = MobileModalFullscreen;


var Style = _styled2.default.div(_templateObject, function (props) {
  return props.wrap ? '400px' : '100%';
}, function (props) {
  return props.wrap ? '5px' : '0px';
}, function (props) {
  return props.wrap ? '60px' : '0';
}, function (props) {
  return props.wrap ? '60px' : '0';
}, function (p) {
  return p.overflowHidden ? 'hidden' : 'auto';
});