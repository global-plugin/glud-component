'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TextArea = (_temp = _class = function (_PureComponent) {
  _inherits(TextArea, _PureComponent);

  function TextArea() {
    _classCallCheck(this, TextArea);

    return _possibleConstructorReturn(this, (TextArea.__proto__ || Object.getPrototypeOf(TextArea)).apply(this, arguments));
  }

  _createClass(TextArea, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          testID = _props.testID,
          className = _props.className,
          onlyContain = _props.onlyContain,
          label = _props.label,
          defaultValue = _props.defaultValue,
          value = _props.value,
          maxLength = _props.maxLength,
          isRequired = _props.isRequired,
          isSuccess = _props.isSuccess,
          isError = _props.isError,
          message = _props.message,
          placeholder = _props.placeholder,
          onChange = _props.onChange,
          onFocus = _props.onFocus,
          onBlur = _props.onBlur,
          onKeyDown = _props.onKeyDown,
          onKeyUp = _props.onKeyUp,
          onKeyPress = _props.onKeyPress,
          disabled = _props.disabled,
          readOnly = _props.readOnly,
          loading = _props.loading,
          rows = _props.rows;


      var status = {
        'is-success': isSuccess,
        'is-danger': isError
      };

      var classInput = (0, _classnames2.default)('textarea', status);
      var classHelp = (0, _classnames2.default)('help', status);
      var classControl = (0, _classnames2.default)('control', {
        'is-loading': loading
      });

      var inputField = _react2.default.createElement('textarea', {
        'data-test-id': testID,
        className: classInput,
        defaultValue: defaultValue,
        value: value,
        maxLength: maxLength,
        placeholder: placeholder,
        onChange: onChange,
        onFocus: onFocus,
        onBlur: onBlur,
        onKeyDown: onKeyDown,
        onKeyUp: onKeyUp,
        onKeyPress: onKeyPress,
        disabled: disabled,
        readOnly: readOnly,
        rows: rows
      });

      if (onlyContain) {
        return _react2.default.createElement(
          'div',
          { className: classControl },
          inputField
        );
      }

      return _react2.default.createElement(
        'div',
        { className: 'field ' + className },
        _react2.default.createElement(
          'label',
          { className: 'label' },
          label,
          ' ',
          isRequired && _react2.default.createElement('i', { className: 'required fas fa-asterisk' })
        ),
        _react2.default.createElement(
          'div',
          { className: classControl },
          inputField
        ),
        message && _react2.default.createElement(
          'p',
          { className: classHelp },
          message
        )
      );
    }
  }]);

  return TextArea;
}(_react.PureComponent), _class.propTypes = {
  testID: _propTypes2.default.string,
  className: _propTypes2.default.string,
  onlyContain: _propTypes2.default.bool,
  label: _propTypes2.default.string,
  defaultValue: _propTypes2.default.string,
  value: _propTypes2.default.string,
  maxLength: _propTypes2.default.number,
  isRequired: _propTypes2.default.bool,
  isSuccess: _propTypes2.default.bool,
  isError: _propTypes2.default.bool,
  message: _propTypes2.default.string,
  placeholder: _propTypes2.default.string,
  onChange: _propTypes2.default.func,
  onFocus: _propTypes2.default.func,
  onBlur: _propTypes2.default.func,
  onKeyDown: _propTypes2.default.func,
  onKeyUp: _propTypes2.default.func,
  onKeyPress: _propTypes2.default.func,
  disabled: _propTypes2.default.bool,
  readOnly: _propTypes2.default.bool,
  loading: _propTypes2.default.bool
}, _class.defaultProps = {
  testID: 'textarea',
  rows: 4
}, _temp);
exports.default = TextArea;