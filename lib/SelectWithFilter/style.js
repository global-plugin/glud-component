'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.default = function (props) {
  var isSuccess = props.isSuccess,
      isError = props.isError,
      disabled = props.disabled;

  var getColor = function getColor() {
    if (isError) return '#FB6B5B';
    if (isSuccess) return '#26a69a';
    if (disabled) return 'whitesmoke';
    return '#dbdbdb';
  };

  return {
    control: function control(styles, _ref) {
      var isDisabled = _ref.isDisabled,
          isFocused = _ref.isFocused,
          isSelected = _ref.isSelected;
      return _extends({}, styles, {
        borderRadius: 3,
        height: 30,
        minHeight: 30,
        borderColor: getColor()
      });
    },
    singleValue: function singleValue(styles) {
      return _extends({}, styles, {
        color: '#363636'
      });
    },
    placeholder: function placeholder(styles) {
      return _extends({}, styles, {
        color: '#363636'
      });
    }
  };
};