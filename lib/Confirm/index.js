'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp2;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactDom = require('react-dom');

var _Modal = require('../Modal');

var _Modal2 = _interopRequireDefault(_Modal);

var _Button = require('../Button');

var _Button2 = _interopRequireDefault(_Button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Confirm = (_temp2 = _class = function (_Component) {
  _inherits(Confirm, _Component);

  function Confirm() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Confirm);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Confirm.__proto__ || Object.getPrototypeOf(Confirm)).call.apply(_ref, [this].concat(args))), _this), _this.componentDidMount = function () {
      var closeTime = _this.props.closeTime;

      if (closeTime) {
        setTimeout(function () {
          return close();
        }, 1000 * closeTime);
      }
    }, _this.handleClickButton = function (button) {
      if (button.onClick) button.onClick();
      close();
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Confirm, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          testID = _props.testID,
          title = _props.title,
          buttons = _props.buttons,
          content = _props.content,
          closeButton = _props.closeButton,
          closeOnClickOutside = _props.closeOnClickOutside;

      return _react2.default.createElement(
        'div',
        { className: 'box-confirm animated fadeInDown' },
        _react2.default.createElement(
          _Modal2.default,
          {
            testID: testID,
            title: title,
            centered: true,
            closeOnClickOutside: closeOnClickOutside,
            open: true,
            onClose: closeButton && close
          },
          _react2.default.createElement(
            _Modal2.default.Content,
            null,
            content
          ),
          _react2.default.createElement(
            _Modal2.default.Footer,
            null,
            buttons.map(function (button, i) {
              return _react2.default.createElement(
                _Button2.default,
                {
                  key: i,
                  testID: testID + '-' + i,
                  primary: button.primary,
                  onClick: function onClick() {
                    return _this2.handleClickButton(button);
                  }
                },
                button.label
              );
            })
          )
        )
      );
    }
  }]);

  return Confirm;
}(_react.Component), _class.propTypes = {
  testID: _propTypes2.default.string,
  title: _propTypes2.default.string,
  buttons: _propTypes2.default.array.isRequired,
  content: _propTypes2.default.node,
  closeButton: _propTypes2.default.bool,
  closeOnClickOutside: _propTypes2.default.bool
}, _class.defaultProps = {
  testID: 'confirm',
  title: 'Title',
  buttons: [{
    label: 'Cancel',
    onClick: function onClick() {
      return null;
    }
  }, {
    primary: true,
    label: 'Confirm',
    onClick: function onClick() {
      return null;
    }
  }],
  content: _react2.default.createElement(
    'div',
    null,
    'Content is here.'
  ),
  closeButton: true,
  closeOnClickOutside: false
}, _temp2);


function createElemet(properties) {
  var divTarget = document.createElement('div');
  divTarget.id = 'box-react-confirm';
  document.body.appendChild(divTarget);

  (0, _reactDom.render)(_react2.default.createElement(Confirm, properties), divTarget);
}

function open(properties) {
  close().then(function () {
    createElemet(properties);
  });
}

function close() {
  return new Promise(function (resolve) {
    var target = document.getElementById('box-react-confirm');
    if (!target) resolve();

    target.classList.add('alert-fade-out');

    var remove = function remove() {
      target.removeEventListener('webkitAnimationEnd', remove, false);
      target.removeEventListener('animationend', remove, false);
      target.addEventListener('onclick', remove, false);
      target.parentNode.removeChild(target);
      resolve();
    };

    target.addEventListener('webkitAnimationEnd', remove, false);
    target.addEventListener('animationend', remove, false);
    target.addEventListener('onclick', remove, false);
  });
}

exports.default = {
  open: open,
  close: close
};