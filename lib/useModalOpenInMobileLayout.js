'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var getParentClosest = function getParentClosest(elem, selector) {
  // Element.matches() polyfill
  if (!Element.prototype.matches) {
    Element.prototype.matches = Element.prototype.matchesSelector || Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelector || Element.prototype.oMatchesSelector || Element.prototype.webkitMatchesSelector || function (s) {
      var matches = (this.document || this.ownerDocument).querySelectorAll(s),
          i = matches.length;
      while (--i >= 0 && matches.item(i) !== this) {}
      return i > -1;
    };
  }

  // Get the closest matching element
  for (; elem && elem !== document; elem = elem.parentNode) {
    if (elem.matches(selector)) return elem;
  }
  return null;
}; /* eslint-disable */

exports.default = function (ref, show) {
  (0, _react.useEffect)(function () {
    var parent = getParentClosest(ref.current, '#ui-container');

    if (parent && show) {
      var modalChild = parent.getAttribute('data-modalchild');
      if (modalChild) {
        parent.setAttribute('data-modalchild', '' + (Number(modalChild) + 1));
      } else {
        parent.setAttribute('data-modalchild', '1');
      }
      parent.style.overflow = 'hidden';
    }

    return function () {
      if (parent && show) {
        var _modalChild = parent.getAttribute('data-modalchild');
        if (_modalChild === '1') {
          parent.style.overflow = 'auto';
          parent.setAttribute('data-modalchild', '0');
        } else {
          var newModalChild = Number(_modalChild) - 1;
          parent.setAttribute('data-modalchild', '' + newModalChild);
        }
      }
    };
  }, [show]);
};