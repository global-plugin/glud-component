'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _templateObject = _taggedTemplateLiteral(['\n  label: MobileLayout;\n  flex-grow: 1;\n  display: flex;\n  flex-direction: column;\n  width: 100%;\n  height: 100%;\n\n  .ui-header, .ui-footer {\n    flex-shrink: 0;\n  }\n\n  .ui-content {\n    flex-grow: 1;\n    overflow: auto;\n    -webkit-overflow-scrolling: touch;\n  }\n'], ['\n  label: MobileLayout;\n  flex-grow: 1;\n  display: flex;\n  flex-direction: column;\n  width: 100%;\n  height: 100%;\n\n  .ui-header, .ui-footer {\n    flex-shrink: 0;\n  }\n\n  .ui-content {\n    flex-grow: 1;\n    overflow: auto;\n    -webkit-overflow-scrolling: touch;\n  }\n']);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styled = require('@emotion/styled');

var _styled2 = _interopRequireDefault(_styled);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var MobileLayout = function MobileLayout(_ref) {
  var header = _ref.header,
      content = _ref.content,
      footer = _ref.footer;
  return _react2.default.createElement(
    Style,
    null,
    header && _react2.default.createElement(
      'div',
      { className: 'ui-header' },
      header
    ),
    _react2.default.createElement(
      'div',
      { className: 'ui-content', id: 'ui-container' },
      content
    ),
    footer && _react2.default.createElement(
      'div',
      { className: 'ui-footer' },
      footer
    )
  );
};

MobileLayout.propTypes = {
  header: _propTypes2.default.node,
  content: _propTypes2.default.node,
  footer: _propTypes2.default.node
};

var Style = (0, _styled2.default)('div')(_templateObject);

exports.default = MobileLayout;