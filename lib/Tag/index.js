'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Tag = (_temp = _class = function (_React$PureComponent) {
  _inherits(Tag, _React$PureComponent);

  function Tag() {
    _classCallCheck(this, Tag);

    return _possibleConstructorReturn(this, (Tag.__proto__ || Object.getPrototypeOf(Tag)).apply(this, arguments));
  }

  _createClass(Tag, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          testID = _props.testID,
          danger = _props.danger,
          dark = _props.dark,
          warning = _props.warning,
          large = _props.large,
          medium = _props.medium,
          success = _props.success,
          light = _props.light,
          info = _props.info,
          rounded = _props.rounded,
          isDelete = _props.isDelete,
          fullwidth = _props.fullwidth,
          onClick = _props.onClick,
          children = _props.children;


      var classes = (0, _classnames2.default)('tag', {
        'is-danger': danger,
        'is-dark': dark,
        'is-large': large,
        'is-medium': medium,
        'is-info': info,
        'is-light': light,
        'is-success': success,
        'is-warning': warning,
        'is-rounded': rounded,
        'is-delete': isDelete,
        'is-fullwidth': fullwidth
      });

      return _react2.default.createElement(
        'span',
        { 'data-test-id': testID, className: classes, onClick: onClick },
        children
      );
    }
  }]);

  return Tag;
}(_react2.default.PureComponent), _class.propTypes = {
  testID: _propTypes2.default.string,
  danger: _propTypes2.default.bool,
  dark: _propTypes2.default.bool,
  warning: _propTypes2.default.bool,
  large: _propTypes2.default.bool,
  medium: _propTypes2.default.bool,
  success: _propTypes2.default.bool,
  light: _propTypes2.default.bool,
  info: _propTypes2.default.bool,
  rounded: _propTypes2.default.bool,
  isDelete: _propTypes2.default.bool,
  fullwidth: _propTypes2.default.bool,
  onClick: _propTypes2.default.func
}, _class.defaultProps = {
  testID: 'tag'
}, _temp);
exports.default = Tag;